class_name Molecules
extends Node

const MOLECULES_PATH = "res://data/molecules.json"

var molecules: Array = []
var molecules_lexically: Dictionary = {}
var molecules_partitioned: Array = []
var molecules_by_cid: Dictionary = {}
var molecules_by_iupac_name: Dictionary = {}
#var molecules_by_name: Dictionary = {}

var _lexical_regex = RegEx.new()

var _molecule_scene = preload("res://scenes/chem/struct/Molecule.tscn")

func _ready():
    _lexical_regex.compile("([a-zA-Z]{2,}.*)")
    for char_code in range('a'.ord_at(0), 'z'.ord_at(0) + 1):
        var c = "%c" % char_code
        molecules_lexically[c] = []
    
    var file = File.new()
    file.open(MOLECULES_PATH, File.READ)
    var data = parse_json(file.get_as_text())
    for molecule_data in data.values():
        molecules.append(molecule_data)
        molecules_by_cid[int(molecule_data['ids']['cid'])] = molecule_data
        var iupac_name = molecule_data['names']['iupac']
        molecules_by_iupac_name[iupac_name.to_lower()] = molecule_data
        molecule_data['lex'] = _get_lexical_name(iupac_name)
        _register_lexically(molecule_data, iupac_name)

#        if not iupac_name.to_lower() in molecules_by_name:
#            molecules_by_name[iupac_name.to_lower()] = [molecule_data]
        for synonym in molecule_data['names']['synonyms']:
#            if not synonym.to_lower() in molecules_by_name:
#                molecules_by_name[synonym.to_lower()] = []
#            molecules_by_name[synonym.to_lower()].append(molecule_data)
            _register_lexically(molecule_data, synonym)
        
        for aid in molecule_data['atoms']:
            molecule_data['atoms'][aid]['valence'] = 0
        for bond_data in molecule_data['bonds']:
            molecule_data['atoms'][String(bond_data['id1'])]['valence'] += bond_data['order']
            molecule_data['atoms'][String(bond_data['id2'])]['valence'] += bond_data['order']
    
    _sort_lexically(molecules)
    for i in range(0, molecules.size()):
        molecules[i]['index'] = i

    for c in molecules_lexically:
        _sort_lexically(molecules_lexically[c])

    molecules_partitioned = _partition_lexically(molecules_lexically)

func _register_lexically(molecule_data: Dictionary, name: String):
        var lex: String = _get_lexical_name(name)
        var c: String = lex.left(1)
        molecules_lexically[c].append({
            'name': name,
            'lex': lex,
            'data': molecule_data
        })

func _get_lexical_name(name: String) -> String:
    var result = _lexical_regex.search(name)
    if result:
        return result.get_string().to_lower()
    else:
        return ''

func create_molecule(index: int, conformer_index: int) -> Molecule:
    var molecule_data: Dictionary = molecules[index]
    return _create_molecule(molecule_data, conformer_index)


func create_molecule_by_pubchem_id(cid: int, conformer_index: int) -> Molecule:
    var molecule_data: Dictionary = molecules_by_cid[cid]
    return _create_molecule(molecule_data, conformer_index)


func create_molecule_by_iupac_name(iupac_name: String, conformer_index: int) -> Molecule:
    var molecule_data: Dictionary = molecules_by_iupac_name[iupac_name]
    return _create_molecule(molecule_data, conformer_index)
    
    
func _create_molecule(molecule_data, conformer_index: int):
    var molecule: Molecule = _molecule_scene.instance()
    molecule.data = molecule_data
    var conformer_id: String = molecule_data['conformers'].keys()[conformer_index]
    print("Conformer index: %s, id: %s" % [conformer_index, conformer_id])
    var conformer_data: Dictionary = molecule_data['conformers'][conformer_id]
    for aid in conformer_data['atoms'].keys():
        var atom_data: Dictionary = _create_conformer_atom_data(molecule_data, conformer_data, aid)
        var atom: Atom = Elements.create_atom(atom_data)
        atom.apply_vdv_radius(false)
        molecule.add_atom(atom)
    for bi in range(0, molecule_data['bonds'].size()):
        var bond_data: Dictionary = _create_conformer_bond_data(molecule, bi)
        var bond: Bond = Elements.create_bond(bond_data);
        molecule.add_bond(bond)
    for bond in molecule.get_node('Bonds').get_children():
        bond.create_sticks()
        
    return molecule

func _create_conformer_atom_data(molecule_data, conformer_data, aid):
    var atom_data = {
        'id': aid,
        'number': molecule_data['atoms'][aid]['number'],
        'valence': molecule_data['atoms'][aid]['valence'],
        'coords': {
            'x': conformer_data['atoms'][aid]['x'],
            'y': conformer_data['atoms'][aid]['y'],
            'z': conformer_data['atoms'][aid]['z']
        },
        'bonds': [],
        'bonds1': [],
        'bonds2': []
    }
    return atom_data

func _create_conformer_bond_data(molecule, bi):
    var bond_data = {
        'atom1': molecule.atoms[String(molecule.data['bonds'][bi]['id1'])],
        'atom2': molecule.atoms[String(molecule.data['bonds'][bi]['id2'])],
        'order': molecule.data['bonds'][bi]['order']
    }
    return bond_data
    
func _sort_lexically(entries):
    entries.sort_custom(self, '_compare_lexically')
    
func _compare_lexically(a, b) -> bool:
    return a['lex'] < b['lex']

func _partition_lexically(by_chararacter: Dictionary, n: int = 10):
    var total: int = 0
    for c in by_chararacter:
        total += len(by_chararacter[c])
    var part_size: int = total / n

    print("total items: %s; partition size: %s" % [total, part_size])
    
    var parts = []
    var part = {
        'from': 'a',
        'to': 'a',
        'items' : []
    }
    Collections.append_all(part['items'], by_chararacter['a'])
    var current_part_count = len(by_chararacter['a'])
    for i in range(1, len(by_chararacter)):
        var c: String = by_chararacter.keys()[i]
        var count: int = len(by_chararacter[c])
        if current_part_count + count > part_size and len(parts) < n - 1:
            parts.append(part)
            part = {
                'from': c,
                'to': c,
                'items': []
            }
            current_part_count = len(by_chararacter[c])
        else:
            part['to'] = c
            current_part_count += count
        
        Collections.append_all(part['items'], by_chararacter[c])

    if len(part['items']) > 0:
        parts.append(part)
        
#    for part in parts:
#        print("%s - %s: %s" % [part['from'], part['to'], len(part['items'])])

    return parts
