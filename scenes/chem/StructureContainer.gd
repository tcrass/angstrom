class_name StructureContainer
extends Spatial

const ROTATION_TWEEN_DURATION = 0.4
const OMEGA_MAX = 30

const AUDIO_VOLUME_TWEEN_DURATION = 1

var structure: Structure setget _set_structure

var _v_theta: float = 0
var _v_phi: float = 0

var _rotating_left: bool = false
var _auto_rotating_left: bool = false
var _rotating_right: bool = false
var _auto_rotating_right: bool = false

func _set_structure(value: Structure):
    if value != structure:
        structure = value
        center()

func _ready():
    MessageBus.subscribe(self, Messages.ROTATE_UP, '_on_rotateUp')
    MessageBus.subscribe(self, Messages.ROTATE_LEFT, '_on_rotateLeft')
    MessageBus.subscribe(self, Messages.AUTO_ROTATE_LEFT, '_on_autoRotateLeft')
    MessageBus.subscribe(self, Messages.ROTATE_RIGHT, '_on_rotateRight')
    MessageBus.subscribe(self, Messages.AUTO_ROTATE_RIGHT, '_on_autoRotateRight')
    MessageBus.subscribe(self, Messages.ROTATE_DOWN, '_on_rotateDown')
    MessageBus.subscribe(self, Messages.MUSIC_VOLUME_CHANGED, '_on_musicVolumeChanged')
    
    center()

func _process(delta: float):
    var xform: Transform = $Scaled.global_transform
    xform.basis = xform.basis.rotated(Vector3.RIGHT, OMEGA_MAX * deg2rad(delta * _v_phi)).rotated(Vector3.UP, OMEGA_MAX * deg2rad(delta * _v_theta))
    $Scaled.global_transform = xform

func center():
    if structure and $"Scaled/Centered":
        for child in $"Scaled/Centered".get_children():
            (child as Node).queue_free()
        $"Scaled/Centered".add_child(structure)
        var d: Vector3 = structure.get_offset()
        $"Scaled/Centered".translation = -d
        $Scaled.scale = structure.get_scale()

func get_radius() -> float:
    var e = get_extent()
    var r = e.size.length() / 2
    return r

func get_extent() -> AABB:
    var s: AABB
    if structure:
        var e = structure.get_extent()
        s = AABB(e.position/2, e.size/2)
    else:
        s = AABB()
    return s

func _on_rotateUp(on: bool):
    _rotate_vertically(-1 if on else 0)
   
func _on_rotateLeft(on: bool):
    _register_rotation(true, false, false, false)
    _rotate_horizontally(-1 if on else 0)
   
func _on_autoRotateLeft(on: bool):
    if on:
        _register_rotation(false, true, false, false)
        _rotate_horizontally(-1)
    else:
        _auto_rotating_left = false
        if not (_rotating_left or _rotating_right or _auto_rotating_right):
            _rotate_horizontally(0)
            
   
func _on_rotateRight(on: bool):
    _register_rotation(false, false, true, false)
    _rotate_horizontally(1 if on else 0)
   
func _on_autoRotateRight(on: bool):
    if on:
        _register_rotation(false, false, false, true)
        _rotate_horizontally(1)
    else:
        _auto_rotating_right = true
        if not (_rotating_left or _rotating_right or _auto_rotating_left):
            _rotate_horizontally(0)
   
func _on_rotateDown(on: bool):
    _rotate_vertically(1 if on else 0)

func _register_rotation(left: bool, auto_left: bool, right: bool, auto_right: bool):
    _rotating_left = left
    _auto_rotating_left = auto_left
    _rotating_right = right
    _auto_rotating_right = auto_right   

func _rotate_horizontally(v: float):
    $Tween.stop(self, '_v_theta')
    $Tween.interpolate_property(self, '_v_theta', _v_theta, v, ROTATION_TWEEN_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
    $Tween.start()

func _rotate_vertically(v: float):
    $Tween.stop(self, '_v_phi')
    $Tween.interpolate_property(self, '_v_phi', _v_phi, v, ROTATION_TWEEN_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
    $Tween.start()

func _on_musicVolumeChanged(volume: float):
    var target_db: float = -100 if volume == 0 else 6 + 36*(volume/100 - 1)
    print("Music target db: %s" % target_db)
    $Tween.stop($AudioPlayer, 'unit_db')
    $Tween.interpolate_property($AudioPlayer, 'unit_db', $AudioPlayer.unit_db, target_db, AUDIO_VOLUME_TWEEN_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
    $Tween.start()
