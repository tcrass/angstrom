extends Node

export var track_camera: bool = false
export var delta_pos: Vector3 = Vector3.ZERO
export var delta_theta: float = 0
export var delta_phi: float = 0
export var delta_r: float = 1
export var track_x: bool = true
export var track_y: bool = true
export var track_z: bool = true
export var track_theta: bool = false
export var track_phi: bool = false
#export var track_sky: bool = false

var target
var _target
var _target_xform0: Transform
var _target_rot0: Vector2

func _ready():
    pass
    
func _process(delta: float):
    var parent = get_parent() as Spatial
    if parent:
        var target = _get_target()
        if target != _target:
            _probe(target)
        _target = target
        if _target:
            var xform: Transform = Transform(_target_xform0.basis, _target_xform0.origin)
            var rot: Vector2 = Vector2(_target_rot0)

            var target_xform: Transform = _target.global_transform

            if track_x:
                xform.origin.x += (target_xform.origin.x - _target_xform0.origin.x)
            if track_y:
                xform.origin.y += (target_xform.origin.y - _target_xform0.origin.y)
            if track_z:
                xform.origin.z += (target_xform.origin.z - _target_xform0.origin.z)
            xform.origin += delta_pos

            if track_theta or track_phi:
                var target_rot: Vector2 = Utils3D.basis2rot(target_xform.basis)
                if track_theta:
                    rot.x += (target_rot.x - _target_rot0.x)
                if track_phi:
                    rot.y += (target_rot.y - _target_rot0.y)
            rot += Vector2(delta_theta, delta_phi)
            xform.basis = Utils3D.rot2basis(rot)

            xform.origin -= delta_r * xform.basis.z

            parent.global_transform = xform.orthonormalized()

func probe_target():
    _probe(_get_target())

func _probe(target):
    if target:
        _target_xform0 = (target as Spatial).global_transform
        _target_rot0 = Utils3D.basis2rot(_target_xform0.basis)

func _get_target():
    if track_camera:
        return get_viewport().get_camera()
    else:
        return target
