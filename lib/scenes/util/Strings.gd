extends Node

func get_char_code(char_str: String) -> int:
    if char_str:
        return char_str.ord_at(0)
    else:
        return -1

func from_char_code(char_code: int) -> String:
    return "%c" % char_code
