class_name GazeButton
extends Button

func _ready():
    set_meta(VRControls.ACTIVATION_DELAY, 0)
    set_meta(VRControls.PRESS_ON_ACTIVATION, true)
