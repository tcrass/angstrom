class_name SpriteButton
extends AbstractGazeActivatedButton

const NORMAL = 1
const FOCUSED = 2
const PRESSED = 4
const DISABLED = 8

export var pixel_size: float = 0.01 setget _set_pixel_size
export var rel_align_x: float = 0 setget _set_rel_align_x
export var rel_align_y: float = 0 setget _set_rel_align_y

export(int, FLAGS, "Normal", "Focussed", "Pressed", "Disabled") var emissive = 0

export var material: SpatialMaterial = create_default_material() setget _set_material
export var texture: Texture setget _set_texture
export var color: Color = Color('ffffff') setget _set_color
export var brightness: float = 0.25 setget _set_brightness
export var emission_texture: Texture setget _set_texture
export var emission_color: Color = MeshSprite.UNDEF_COLOR setget _set_emission_color
export var emission_energy: float = 0 setget _set_emission_energy

export var focussed_material: SpatialMaterial setget _set_focussed_material
export var focussed_texture: Texture setget _set_focussed_texture
export var focussed_color: Color = MeshSprite.UNDEF_COLOR setget _set_focussed_color
export var focussed_brightness: float = 0.75 setget _set_focussed_brightness
export var focussed_emission_texture: Texture setget _set_focussed_emission_texture
export var focussed_emission_color: Color = MeshSprite.UNDEF_COLOR setget _set_focussed_emission_color
export var focussed_emission_energy: float = 0 setget _set_focussed_emission_energy

export var pressed_material: SpatialMaterial setget _set_pressed_material
export var pressed_texture: Texture setget _set_pressed_texture
export var pressed_color: Color = MeshSprite.UNDEF_COLOR setget _set_pressed_color
export var pressed_brightness: float = 0.5 setget _set_pressed_brightness
export var pressed_emission_texture: Texture setget _set_pressed_texture
export var pressed_emission_color: Color = MeshSprite.UNDEF_COLOR setget _set_pressed_emission_color
export var pressed_emission_energy: float = 1 setget _set_pressed_emission_energy

export var disabled_material: SpatialMaterial setget _set_disabled_material
export var disabled_texture: Texture setget _set_disabled_texture
export var disabled_color: Color = MeshSprite.UNDEF_COLOR setget _set_disabled_color
export var disabled_brightness: float = -0.25 setget _set_disabled_brightness
export var disabled_emission_texture: Texture setget _set_disabled_emission_texture
export var disabled_emission_color: Color = MeshSprite.UNDEF_COLOR setget _set_disabled_emission_color
export var disabled_emission_energy: float = 0 setget _set_disabled_emission_energy

func _set_disabled(value):
    ._set_disabled(value)
    if value != disabled:
        if $CollisionShape:
            $CollisionShape.disabled = disabled

func _set_pixel_size(value: float):
    if value != pixel_size:
        pixel_size = value
        update_display()
        
func _set_rel_align_x(value: float):
    rel_align_x = value
    update_display()

func _set_rel_align_y(value: float):
    rel_align_y = value
    update_display()
    
func _set_material(value: SpatialMaterial):
    if value != material:
        if material:
            material = value.duplicate()
        else:
            material = create_default_material()
        update_display()
        
func _set_texture(value: Texture):
    if value != texture:
        texture = value
        update_display()
        
func _set_color(value: Color):
    if value != color:
        color = value
        update_display()

func _set_brightness(value: float):
    if value != brightness:
        brightness = value
        update_display()

func _set_emission_texture(value: Texture):
    if value != emission_texture:
        emission_texture = value
        update_display()
        
func _set_emission_color(value: Color):
    if value != emission_color:
        emission_color = value
        update_display()

func _set_emission_energy(value: float):
    if value != emission_energy:
        emission_energy = value
        update_display()

func _set_focussed_material(value: SpatialMaterial):
    if value != focussed_material:
        focussed_material = value
        update_display()
        
func _set_focussed_texture(value: Texture):
    if value != focussed_texture:
        focussed_texture = value
        update_display()
        
func _set_focussed_color(value: Color):
    if value != focussed_color:
        focussed_color = value
        update_display()

func _set_focussed_brightness(value: float):
    if value != focussed_brightness:
        focussed_brightness = value
        update_display()

func _set_focussed_emission_texture(value: Texture):
    if value != focussed_emission_texture:
        focussed_emission_texture = value
        update_display()
        
func _set_focussed_emission_color(value: Color):
    if value != focussed_emission_color:
        focussed_emission_color = value
        update_display()

func _set_focussed_emission_energy(value: float):
    if value != focussed_emission_energy:
        focussed_emission_energy = value
        update_display()

func _set_pressed_material(value: SpatialMaterial):
    if value != pressed_material:
        pressed_material = value
        update_display()
        
func _set_pressed_texture(value: Texture):
    if value != pressed_texture:
        pressed_texture = value
        update_display()
        
func _set_pressed_color(value: Color):
    if value != pressed_color:
        pressed_color = value
        update_display()

func _set_pressed_brightness(value: float):
    if value != pressed_brightness:
        pressed_brightness = value
        update_display()

func _set_pressed_emission_texture(value: Texture):
    if value != pressed_emission_texture:
        pressed_emission_texture = value
        update_display()
        
func _set_pressed_emission_color(value: Color):
    if value != pressed_emission_color:
        pressed_emission_color = value
        update_display()

func _set_pressed_emission_energy(value: float):
    if value != pressed_emission_energy:
        pressed_emission_energy = value
        update_display()

func _set_disabled_material(value: SpatialMaterial):
    if value != disabled_material:
        disabled_material = value
        update_display()
        
func _set_disabled_texture(value: Texture):
    if value != disabled_texture:
        disabled_texture = value
        update_display()
        
func _set_disabled_color(value: Color):
    if value != disabled_color:
        disabled_color = value
        update_display()

func _set_disabled_brightness(value: float):
    if value != disabled_brightness:
        disabled_brightness = value
        update_display()

func _set_disabled_emission_texture(value: Texture):
    if value != disabled_emission_texture:
        disabled_emission_texture = value
        update_display()
        
func _set_disabled_emission_color(value: Color):
    if value != disabled_emission_color:
        disabled_emission_color = value
        update_display()

func _set_disabled_emission_energy(value: float):
    if value != disabled_emission_energy:
        disabled_emission_energy = value
        update_display()

static func create_default_material() -> SpatialMaterial:
    var default_material = MeshSprite.create_default_material()
    default_material.flags_transparent = true
    return default_material

func _ready():
    ._ready()
    update_display()

func update_display():
    if disabled:
        _display_as_disabled()
    else:
        match mode:
            Mode.ACTION_BUTTON:
                _update_display_on_pressed()
            Mode.PUSH_BUTTON:
                _update_display_on_focussed()
            Mode.TOGGLE_BUTTON:
                _update_display_on_pressed()

func _update_display_on_pressed():
    if color:
        if pressed:
            _display_as_pressed()
        else:
            _update_display_on_focussed()

func _update_display_on_focussed():
    if color:
        if focused:
            _display_as_focussed()
        else:
            _display_as_normal()

func _display_as_disabled():
    _update_display_by(
        _determine_disabled_material(),
        _determine_disabled_texture(),
        _determine_disabled_color(),
        _determine_disabled_brightness(),
        emissive & DISABLED,
        _determine_disabled_emission_texture(),
        _determine_disabled_emission_color(),
        1   
    )

func _display_as_normal():
    _update_display_by(
        _determine_material(),
        _determine_texture(),
        _determine_color(),
        _determine_brightness(),
        emissive & NORMAL,
        _determine_emission_texture(),
        _determine_emission_color(),
        1   
    )

func _display_as_focussed():
    _update_display_by(
        _determine_focussed_material(),
        _determine_focussed_texture(),
        _determine_focussed_color(),
        _determine_focussed_brightness(),
        emissive & FOCUSED,
        _determine_focussed_emission_texture(),
        _determine_focussed_emission_color(),
        1   
    )

func _display_as_pressed():
    _update_display_by(
        _determine_pressed_material(),
        _determine_pressed_texture(),
        _determine_pressed_color(),
        _determine_pressed_brightness(),
        emissive & PRESSED,
        _determine_pressed_emission_texture(),
        _determine_pressed_emission_color(),
        1   
    )


func _update_display_by(
        material: SpatialMaterial, 
        texture: Texture, 
        color: Color,
        brightness: float,
        emissive: bool,
        emission_texture: Texture,
        emission_color: Color,
        emission_energy: float
    ):
        if $Sprite:
            $Sprite.pixel_size = pixel_size
            $Sprite.rel_align_x = rel_align_x
            $Sprite.rel_align_y = rel_align_y
            $Sprite.material = material
            $Sprite.texture = texture
            $Sprite.color = Colors.alter_brightness(color, brightness) if brightness >= 0 else color
            $Sprite.emissive = emissive
            $Sprite.emission_texture = emission_texture
            $Sprite.emission_color = emission_color
            $Sprite.emission_energy = emission_energy
        
            var size: Vector2 = $Sprite.get_size()
            $CollisionShape.shape.extents = Vector3(size.x/2, size.y/2, 0.01)    
            $CollisionShape.translation = Vector3(rel_align_x * size.x/2, rel_align_y * size.y/2, 0)
        
            
func _determine_disabled_material() -> SpatialMaterial:
    return _find_first_defined([
            disabled_material, 
            material
        ], null, null)
    
func _determine_material() -> SpatialMaterial:
    return _find_first_defined([
            material, 
            disabled_material
        ], null, null)
    
func _determine_focussed_material() -> SpatialMaterial:
    return _find_first_defined([
            focussed_material, 
            material, 
            disabled_material
        ], null, null)
    
func _determine_pressed_material() -> SpatialMaterial:
    return _find_first_defined([
            pressed_material, 
            focussed_material, 
            material, 
            disabled_material
        ], null, null)
    

func _determine_disabled_texture() -> Texture:
    return _find_first_defined([
            disabled_texture, 
            texture
        ], null, null)
    
func _determine_texture() -> Texture:
    return _find_first_defined([
            texture, 
            disabled_texture
        ], null, null)
    
func _determine_focussed_texture() -> Texture:
    return _find_first_defined([
            focussed_texture, 
            texture, 
            disabled_texture
        ], null, null)
    
func _determine_pressed_texture() -> Texture:
    return _find_first_defined([
            pressed_texture, 
            focussed_texture, 
            texture, 
            disabled_texture
        ], null, null)
   

func _determine_disabled_color() -> Color:
    return _find_first_defined([
            disabled_color, 
            color
        ], MeshSprite.UNDEF_COLOR, MeshSprite.DEFAULT_COLOR)
    
func _determine_color() -> Color:
    return _find_first_defined([
            color, 
            disabled_color
        ], MeshSprite.UNDEF_COLOR, MeshSprite.DEFAULT_COLOR)
    
func _determine_focussed_color() -> Color:
    return _find_first_defined([
            focussed_color, 
            color, 
            disabled_color
        ], MeshSprite.UNDEF_COLOR, MeshSprite.DEFAULT_COLOR)
    
func _determine_pressed_color() -> Color:
    return _find_first_defined([
            pressed_color, 
            focussed_color, 
            color, 
            disabled_color
        ], MeshSprite.UNDEF_COLOR, MeshSprite.DEFAULT_COLOR)
    
func _determine_disabled_brightness() -> float:
    return _find_first_float_ge_zero([
            disabled_brightness,
            brightness,
        ], -1)
    
func _determine_brightness() -> float:
    return _find_first_float_ge_zero([
            brightness, 
            disabled_brightness
        ], -1)
    
func _determine_focussed_brightness() -> float:
    return _find_first_float_ge_zero([
            focussed_brightness, 
            brightness, 
            disabled_brightness
        ], -1)
    
func _determine_pressed_brightness() -> float:
    return _find_first_float_ge_zero([
            pressed_brightness, 
            focussed_brightness, 
            brightness, 
            disabled_brightness
        ], -1)
        
func _determine_disabled_emission_texture() -> Texture:
    return _find_first_defined([
            disabled_emission_texture, 
            emission_texture,
            _determine_disabled_texture()
        ], null, null)
    
func _determine_emission_texture() -> Texture:
    return _find_first_defined([
            emission_texture, 
            disabled_emission_texture, 
            _determine_texture()
        ], null, null)
    
func _determine_focussed_emission_texture() -> Texture:
    return _find_first_defined([
            focussed_emission_texture, 
            emission_texture, 
            disabled_emission_texture, 
            _determine_focussed_texture()
        ], null, null)
    
func _determine_pressed_emission_texture() -> Texture:
    return _find_first_defined([
            pressed_texture, 
            focussed_texture, 
            texture, disabled_texture, 
            _determine_pressed_texture()
        ], null, null)
    
func _determine_disabled_emission_color() -> Color:
    return _find_first_defined([
            disabled_emission_color, 
            emission_color, 
            _determine_disabled_color()
        ], MeshSprite.UNDEF_COLOR, MeshSprite.DEFAULT_COLOR)
        
func _determine_emission_color() -> Color:
    return _find_first_defined([
            emission_color, 
            disabled_emission_color, 
            _determine_disabled_color()
        ], MeshSprite.UNDEF_COLOR, MeshSprite.DEFAULT_COLOR)
    
func _determine_focussed_emission_color() -> Color:
    return _find_first_defined([
            focussed_emission_color, 
            emission_color, 
            disabled_emission_color, 
            _determine_focussed_color()
        ], MeshSprite.UNDEF_COLOR, MeshSprite.DEFAULT_COLOR)
    
func _determine_pressed_emission_color() -> Color:
    return _find_first_defined([
            pressed_color, 
            focussed_color, 
            color, disabled_color, 
            _determine_pressed_color()
        ], MeshSprite.UNDEF_COLOR, MeshSprite.DEFAULT_COLOR)
        
func _determine_disabled_emission_energy() -> float:
    return _find_first_float_ge_zero([
            disabled_emission_energy,
            emission_energy,
        ], 1)
    
func _determine_emission_energy() -> float:
    return _find_first_float_ge_zero([
            brightness, 
            disabled_brightness
        ], 1)
    
func _determine_focussed_emission_energy() -> float:
    return _find_first_float_ge_zero([
            focussed_emission_energy, 
            emission_energy, 
            disabled_emission_energy
        ], 1)
    
func _determine_pressed_emission_energy() -> float:
    return _find_first_float_ge_zero([
            pressed_emission_energy, 
            focussed_emission_energy, 
            emission_energy, 
            disabled_emission_energy
        ], 1)
        
func _find_first_defined(values: Array, undef, default):
    var result = default
    for value in values:
        if value != undef:
            result = value
            break
    return result 
    
func _find_first_float_ge_zero(values: Array, default: float):
    var result = default
    for value in values:
        if value >= 0:
            result = value
            break
    return result 
    
