class_name Structure
extends Spatial

var preferred_scale: float = 1
var data: Dictionary = {}


func get_scale() -> Vector3:
    var s: Vector3 = Vector3.ONE * preferred_scale
    return s

func get_offset() -> Vector3:
    var e: AABB = get_extent()
    var o: Vector3 = e.position + e.size/2
    return o

func get_extent() -> AABB:
    return AABB()
