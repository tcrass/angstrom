class_name StructureHUDPanel
extends Panel

export var top_left: String setget _set_top_left, _get_top_left
export var top_right: String setget _set_top_right, _get_top_right
export var bottom_left: String setget _set_bottom_left, _get_bottom_left
export var bottom_right: String setget _set_bottom_right, _get_bottom_right

func _set_top_left(value: String):
    find_node("TopLeft").text = value
    
func _get_top_left() -> String:
    return find_node("TopLeft").text

func _set_top_right(value: String):
    find_node("TopRight").text = value
    
func _get_top_right() -> String:
    return find_node("TopRight").text

func _set_bottom_left(value: String):
    find_node("BottomLeft").text = value
    
func _get_bottom_left() -> String:
    return find_node("BottomLeft").text

func _set_bottom_right(value: String):
    find_node("BottomRight").text = value
    
func _get_bottom_right() -> String:
    return find_node("BottomRight").text

