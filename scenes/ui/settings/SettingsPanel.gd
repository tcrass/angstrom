class_name SettingsPanel
extends PanelContainer

onready var _sunlightButton = $MarginContainer/Sections/LightingMode/LightingModeButtons/SunlightButton
onready var _sunsetButton = $MarginContainer/Sections/LightingMode/LightingModeButtons/SunsetButton
onready var _moonlightButton = $MarginContainer/Sections/LightingMode/LightingModeButtons/MoonlightButton
onready var _volumeSlider = $MarginContainer/Sections/Audio/VolumeSlider
onready var _musicVolumeSlider = $MarginContainer/Sections/Audio/MusicVolumeSlider

func _ready():
    MessageBus.subscribe(self, Messages.LIGHTING_MODE_CHANGED, "_on_lightingModeChanged")
    MessageBus.subscribe(self, Messages.VOLUME_CHANGED, "_on_volumeChanged")
    MessageBus.subscribe(self, Messages.MUSIC_VOLUME_CHANGED, "_on_musicVolumeChanged")

func _on_OkButton_pressed():
    MessageBus.publish(Messages.HIDE_SETTINGS)

func _on_SunlightButton_pressed():
    MessageBus.publish(Messages.LIGHTING_MODE_CHANGED, LightingMode.SUNLIGHT)

func _on_SunsetButton_pressed():
    MessageBus.publish(Messages.LIGHTING_MODE_CHANGED, LightingMode.SUNSET)

func _on_MoonlightButton_pressed():
    MessageBus.publish(Messages.LIGHTING_MODE_CHANGED, LightingMode.MOONLIGHT)

func _on_VolumeSlider_value_changed(value):
    MessageBus.publish(Messages.VOLUME_CHANGED, value)

func _on_MusicVolumeSlider_value_changed(value):
    MessageBus.publish(Messages.MUSIC_VOLUME_CHANGED, value)

func _on_lightingModeChanged(lightingMode):
    match lightingMode:
        LightingMode.SUNLIGHT:
            if not _sunlightButton.pressed:
                _sunlightButton.pressed = true
        LightingMode.SUNSET:
            if not _sunsetButton.pressed:
                _sunsetButton.pressed = true
        LightingMode.MOONLIGHT:
            if not _moonlightButton.pressed:
                _moonlightButton.pressed = true

func _on_volumeChanged(value):
    if _volumeSlider.value != value:
        _volumeSlider.value = value
    
func _on_musicVolumeChanged(value):
    if _musicVolumeSlider.value != value:
        _musicVolumeSlider.value = value
