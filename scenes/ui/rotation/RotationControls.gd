class_name RotationControls
extends Spatial

func _ready():
    $Up.collision_layer = Constants.CL_CONTROLS
    $Left.collision_layer = Constants.CL_CONTROLS
    $Right.collision_layer = Constants.CL_CONTROLS
    $Down.collision_layer = Constants.CL_CONTROLS
    hide()

func show():
    $Up.show()
    $Left.show()
    $Right.show()
    $Down.show()
    
func hide():
    $Up.hide()
    $Left.hide()
    $Right.hide()
    $Down.hide()
