class_name RadioGroup
extends Node

signal selected_changed(button)

var buttons: Array = []
var selected setget _set_selected

func _set_selected(value):
    if value != selected:
        selected = value
        for button in buttons:
            if button != value:
                button.pressed = false
        value.pressed = true
        emit_signal("selected_changed", value)

func register(button):
    buttons.append(button)

func unregister(button):
    buttons.remove(button)
