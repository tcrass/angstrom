class_name Structures
extends Node

var _index: int = 0
var _conformerIndex: int = 0

var _displayMode = Molecule.DisplayMode.SPACE_FILLING


func _ready():
    MessageBus.subscribe(self, Messages.SELECT_PREV, '_on_selectPrev')
    MessageBus.subscribe(self, Messages.SELECT_RANDOM, '_on_selectRandom')
    MessageBus.subscribe(self, Messages.SELECT_NEXT, '_on_selectNext')
    MessageBus.subscribe(self, Messages.SELECT_MOLECULE, '_on_selectMolecule')
    MessageBus.subscribe(self, Messages.SELECT_CONFORMER, '_on_selectConformer')
    MessageBus.subscribe(self, Messages.SELECT_DISPLAY_MODE, "_on_selectDisplayMode")

func select_prev():
    _conformerIndex = 0
    _index -= 1
    if _index < 0:
        _index = len($Molecules.molecules) - 1
    _select_molecule(_index, _conformerIndex)

func select_next():
    _conformerIndex = 0
    _index += 1
    if _index >= len($Molecules.molecules):
        _index = 0
    _select_molecule(_index, _conformerIndex)
    
func select_random():
    _conformerIndex = 0
    _index = int(rand_range(0, len($Molecules.molecules)))
    _select_molecule(_index, _conformerIndex)
   

func select_by_pubchem_id(cid: int):
    var mol = $Molecules.molecules_by_cid[cid]
    _index = $Molecules.molecules_by_cid[cid]['index']
    _select_molecule(_index, _conformerIndex)

func _select_molecule(index: int, conformerIndex: int):
    var m: Molecule = $Molecules.create_molecule(index, conformerIndex)
    _set_structure(m)
    m.set_display_mode(_displayMode, false)
    
    
func _set_structure(structure):
    $StructureContainer.structure = structure
    var r = $StructureContainer.get_radius()
    $StructureContainer.translation = Vector3(0, 1 + r, -1.5 - r)
    MessageBus.publish(Messages.STRUCTURE_CHANGED, structure)
    MessageBus.publish(Messages.INFO_CHANGED, structure.data)

func _on_selectPrev():
    select_prev()

func _on_selectRandom():
    select_random()

func _on_selectNext():
    select_next()

func _on_selectMolecule(data):
    _conformerIndex = 0
    _index = $Molecules.molecules.find(data)
    _select_molecule(_index, _conformerIndex)

func _on_selectConformer():
    _conformerIndex += 1
    if _conformerIndex >= $StructureContainer.structure.data['conformers'].size():
        _conformerIndex = 0
    _select_molecule(_index, _conformerIndex)
   

func _on_selectDisplayMode(mode):
    _displayMode = mode    
