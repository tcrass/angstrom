extends Spatial
class_name Reticle

signal focused_changed(reticle)
signal activation_progress_changed(reticle)
signal activation_complete_changed(reticle)

export var max_focus_distance: float = 10
export var activation_pre_delay: float = 0.75
export var activation_delay: float = 1.5

export var size: float = 0.1
export var drawing_size: Vector2 = Vector2(256, 256)
export var reticle_painter: PackedScene setget _set_reticle_painter
export var collision_mask: int = 1

var distance: float = max_focus_distance
var focused: bool = false setget _set_focused
var activating: bool = false
var activation_progress: float = 0 setget _set_activation_progress
var activation_complete: bool = false setget _set_activation_complete
var render_priority: int = 8

var _reticle_painter: AbstractReticlePainter
var _looking_at: Node = null
var _target: Node = null
var _activator: GazeActivator = null

func _set_reticle_painter(value):
    if value != reticle_painter:
        reticle_painter = value
        if _reticle_painter:
            disconnect("focused_changed", _reticle_painter, "on_focused_changed")
            disconnect("activation_progress_changed", _reticle_painter, "on_activation_progress_changed")
            disconnect("activation_complete_changed", _reticle_painter, "on_activation_complete_changed")
            _reticle_painter.queue_free()
        if reticle_painter:
            _reticle_painter = reticle_painter.instance() as AbstractReticlePainter
            connect("focused_changed", _reticle_painter, "on_focused_changed")
            connect("activation_progress_changed", _reticle_painter, "on_activation_progress_changed")
            connect("activation_complete_changed", _reticle_painter, "on_activation_complete_changed")
            _update_projector()


func _set_focused(value : bool):
    if value != focused:
        focused = value
        emit_signal("focused_changed", self)

func _set_activation_progress(value : float):
    if value != activation_progress:
        activation_progress = value
        emit_signal("activation_progress_changed", self)
                
func _set_activation_complete(value : bool):
    if value != activation_complete:
        activation_complete = value
        emit_signal("activation_complete_changed", self)

func _ready():
    $PreDelayTimer.connect("timeout", self, "_on_pre_delay_timeout")
    $Tween.connect("tween_completed", self, "_on_activation_complete")
    _update_projector()

func _update_projector():
    if $Projector:
        $Projector.add_child(_reticle_painter)

func _process(delta : float):
    $Projector.size = drawing_size
    #var mat = $Screen.material_override as SpatialMaterial
    $Screen.material_override.albedo_texture = $Projector.get_texture()
    $Screen.texture = $Projector.get_texture()
    $Screen.material_override.render_priority = render_priority
    if _reticle_painter:
        _reticle_painter.drawing_size = drawing_size

    var camera = get_viewport().get_camera()
    if camera and camera.current:
        self.global_transform = camera.global_transform

        $TargetProbe.collision_mask = collision_mask
        $TargetProbe.global_transform = camera.global_transform
        $TargetProbe.cast_to = max_focus_distance * Vector3.FORWARD

        var looking_at : Node = null
        var pointing_at : Vector3
        if $TargetProbe.is_colliding():
            looking_at = $TargetProbe.get_collider()
            pointing_at = $TargetProbe.get_collision_point()
        distance = (pointing_at - to_global(Vector3.ZERO)).length() if $TargetProbe.is_colliding() else max_focus_distance
        $Screen.global_transform = camera.global_transform
        $Screen.scale = Vector3(size * distance, size * distance, 1)
        $Screen.translation = Vector3(0, 0, -distance)

        if looking_at != _looking_at:
            print("Looking at changed to %s, pointing at %s at distance %s" % [looking_at, pointing_at, (pointing_at - self.global_transform.origin).length()])
            _looking_at = looking_at

            if _activator:
                _activator.disconnect("focusable_changed", self, "on_activator_interactivity_changed")
                _activator.disconnect("activatable_changed", self, "on_activator_interactivity_changed")
                _reset_activation()
                _activator.looked_at = false
                _activator.pointing_at = null
                _activator.focused = false

            var activator : GazeActivator = null
            if looking_at:
                activator = NodeFinder.find_descentant(looking_at, funcref(self, "_is_activator"))
                
            if activator != _activator:
                _activator = activator
                if activator:
                    _target = looking_at
                    _activator.connect("focusable_changed", self, "on_activator_interactivity_changed")
                    _activator.connect("activatable_changed", self, "on_activator_interactivity_changed")
                    _activator.looked_at = true
                    print("Target acquired: %s (%s) with activator %s (%s)" % [_target.name, _target, _activator.name, _activator])
                else:
                    _target = null
                    print("No target acquired")

        
        if _activator:
            _activator.pointing_at = pointing_at
            _focus(_activator.focusable)
            _activate(_activator.activatable)
        else:
            _focus(0)
            _activate(0)
          
func _is_activator(node : Node) -> bool:
    return node is GazeActivator

func effective_activation_pre_delay() -> float:
    if _activator and _activator.activation_pre_delay_override >= 0:
        return _activator.activation_pre_delay_override
    else:
        return activation_pre_delay

func effective_activation_delay():
    if _activator and _activator.activation_delay_override >= 0:
        return _activator.activation_delay_override
    else:
        return activation_delay
    
func _focus(value : bool):    
    self.focused = value
    if _activator:
        _activator.focused = value
    
func _activate(value : bool):
    if value != activating:
        if value:
            _start_activation()
        else:
            _reset_activation()
    
func _start_activation():
    _reset_activation()
    self.activating = true
    var pre_delay: float = effective_activation_pre_delay()
    if pre_delay > 0:
        $PreDelayTimer.wait_time = pre_delay
        $PreDelayTimer.start()
    else:
        _on_pre_delay_timeout()
            

func _on_pre_delay_timeout():
    var delay: float = effective_activation_delay()
    if delay > 0:
        $Tween.interpolate_property(self, "activation_progress", 0, 1, delay, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
        $Tween.start()
    else:
        self.activation_progress = 1
        _on_activation_complete(self, "activation_progress")
    
    
func _on_activation_complete(object, key):
    self.activation_progress = 0
    self.activation_complete = 1
    _activator.activate()

func _reset_activation():
    $PreDelayTimer.stop()
    $Tween.stop(self)
    self.activating = false;
    self.activation_progress = 0
    self.activation_complete = 0

func on_activator_interactivity_changed(focusable):
    _focus(_activator.focusable)
    _activate(_activator.activatable)
