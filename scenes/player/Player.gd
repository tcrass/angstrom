extends ARVROrigin
class_name Player

var theta : float = 0
var phi : float  = 0
var position : Vector3 = 3*Vector3.RIGHT + 3*Vector3.UP

func _ready():
    position = transform.origin

func _process(delta : float):
    # Use cursor keys to look around
    var dTheta : float = 0 # Looking left/right
    var dPhi : float = 0   # Looking up/down 
    var dr : float = 0     # Moving forward/backward
    if Input.is_action_pressed("ui_right"):
        dTheta += 0.5
    if Input.is_action_pressed("ui_left"):
        dTheta -= 0.5
    if Input.is_action_pressed("ui_up") and not Input.is_key_pressed(KEY_SHIFT):
        dPhi += 0.5
    if Input.is_action_pressed("ui_down") and not Input.is_key_pressed(KEY_SHIFT):
        dPhi -= 0.5
    if Input.is_action_pressed("move_forward"):
        dr += 2
    if Input.is_action_pressed("move_backward"):
        dr -= 2
    
    theta += delta * dTheta
    phi += delta * dPhi
    position += delta * dr * Vector3.FORWARD.rotated(Vector3.UP, -theta)

    translation = position
    rotation = Vector3(0, -theta, 0)
    $Camera.transform.basis = Basis().rotated(Vector3.RIGHT, phi)
