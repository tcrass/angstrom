class_name LeftPanel
extends PanelContainer

func _ready():
    MessageBus.subscribe(self, Messages.AUTO_ROTATE_LEFT, '_on_autoRotateLeft')
    MessageBus.subscribe(self, Messages.ROTATE_RIGHT, '_on_rotateRight')
    MessageBus.subscribe(self, Messages.AUTO_ROTATE_RIGHT, '_on_autoRotateRight')

func _on_autoRotateLeft(on: bool):
    if on:
        $VBoxContainer/AutoRotateCheckBox.pressed = true

func _on_rotateRight(on: bool):
    $VBoxContainer/AutoRotateCheckBox.pressed = false

func _on_autoRotateRight(on: bool):
    if on:
        $VBoxContainer/AutoRotateCheckBox.pressed = false

func _on_LeftButton_button_down():
    $VBoxContainer/AutoRotateCheckBox.pressed = false    
    MessageBus.publish(Messages.AUTO_ROTATE_RIGHT, false)
    MessageBus.publish(Messages.ROTATE_LEFT, true)

func _on_LeftButton_button_up():
    MessageBus.publish(Messages.ROTATE_LEFT, false)

func _on_AutoRotateCheckBox_toggled(button_pressed):
    MessageBus.publish(Messages.AUTO_ROTATE_LEFT, button_pressed)
