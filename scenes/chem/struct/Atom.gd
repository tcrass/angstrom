class_name Atom
extends StaticBody

const BALL_RADIUS: float = 0.5
const TWEEN_DURATION: float = 1.0

var data: Dictionary setget _set_data
var element_data: Dictionary setget _set_element_data

func _set_data(value):
    data = value
    translation = Vector3(data['coords']['x'], data['coords']['y'], data['coords']['z'])


func _set_element_data(value):
    element_data = value
    _apply_element_data()

func _ready():
    _apply_element_data()
    
func _apply_element_data():
    if element_data && $MeshInstance:
        $MeshInstance.material_override = element_data['materials']['jmol']

func apply_vdv_radius(tween: bool):
    var vdv_radius: float = element_data['vdv_radius']
    set_radius(vdv_radius, tween)

func set_radius(r: float, tween: bool):
    $Tween.stop(self, 'scale')
    if tween:
        $Tween.interpolate_property(self, 'scale', scale, Vector3(r, r, r), TWEEN_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
        $Tween.start()
    else:
        scale = Vector3(r, r, r)

func _on_GazeActivator_looked_at_changed(looked_at):
    if (looked_at):
        MessageBus.publish(Messages.ATOM_CHANGED, self)
    else:
        MessageBus.publish(Messages.ATOM_CHANGED, null)
