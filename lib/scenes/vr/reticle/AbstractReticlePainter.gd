class_name AbstractReticlePainter
extends Node2D

export var drawing_size: Vector2 = Vector2(256, 256)

func _process(delta: float):
    update()

func on_focused_changed(reticle):
    pass

func on_activation_progress_changed(reticle):
    pass
    
func on_activation_complete_changed(reticle):
    pass

func draw():
    pass
