class_name MeshSprite
extends MeshInstance

const UNDEF_COLOR = Color('00000000')
const DEFAULT_COLOR = Color('ffffffff')

export var pixel_size: float = 0.01 setget _set_pixel_size
export var rel_align_x: float = 0 setget _set_rel_align_x
export var rel_align_y: float = 0 setget _set_rel_align_y
export var material: Material = create_default_material() setget _set_material
export var texture: Texture setget _set_texture
export var color: Color = Color('ffffff') setget _set_color
export var emissive: bool = false setget _set_emissive
export var emission_texture: Texture setget _set_texture
export var emission_color: Color = Color('ffffff') setget _set_emission_color
export var emission_energy: float = 1 setget _set_emission_energy

var _texture_size: Vector2 = Vector2.ZERO
var _size: Vector2 = Vector2.ZERO

func _set_pixel_size(value):
    if value != pixel_size:
        pixel_size = value
        update_display()
        
func _set_rel_align_x(value: float):
    rel_align_x = value
    update_display()

func _set_rel_align_y(value: float):
    rel_align_y = value
    update_display()

func _set_material(value: Material):
    if value != material:
        if material:
            material = value.duplicate()
        else:
            material = create_default_material()
        update_display()
        
func _set_texture(value):
    if value != texture:
        texture = value
        update_display()
        
func _set_color(value):
    if value != color:
        color = value
        update_display()

func _set_emissive(value: bool):
    if value != emissive:
        emissive = value
        update_display()
        
func _set_emission_texture(value):
    if value != emission_texture:
        emission_texture = value
        update_display()
        
func _set_emission_color(value):
    if value != emission_color:
        emission_color = value
        update_display()

func _set_emission_energy(value):
    if value != emission_energy:
        emission_energy = value
        update_display()

static func create_default_material() -> SpatialMaterial:
    var default_material = SpatialMaterial.new()
    default_material.flags_transparent = true
    return default_material

func _ready():
    update_display()

func _process(delta: float):
    update_display()

func update_display():
    var smaterial = material as SpatialMaterial
    if smaterial:
        if not texture:
            texture = smaterial.albedo_texture
        if not emission_texture:
            emission_texture = smaterial.emission_texture
    
        smaterial.albedo_texture = texture
        smaterial.albedo_color = color
        smaterial.emission_enabled = emissive
        if emissive:
            smaterial.emission_texture = emission_texture
            smaterial.emission = emission_color
            smaterial.emission_energy = emission_energy
    
    material_override = material

    if texture:
        _texture_size = texture.get_size()
    else:
        if emission_texture:
            _texture_size = emission_texture.get_size()
        else:
            _texture_size = Vector2.ZERO

    _size = Vector2(pixel_size * _texture_size.x, pixel_size * _texture_size.y)
    scale = Vector3(_size.x, _size.y, 1)
    translation = Vector3(rel_align_x * _size.x/2, rel_align_y * _size.y/2, 0)

func get_texture_size() -> Vector2:
    return _texture_size

func get_size() -> Vector2:
    return _size

