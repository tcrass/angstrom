class_name VRPanel
extends Panel3D

var CONTROL_UNDER_MOUSE_FILTER = funcref(self, "_is_under_mouse")

var HANDLERS = {
        'Container': preload("handlers/NoOpHandler.tscn").instance(),
        'Control': preload("handlers/DefaultControlHandler.tscn").instance(),
        'Button': preload("handlers/ButtonHandler.tscn").instance(),
        'ItemList': preload("handlers/ItemListHandler.tscn").instance(),
        'ScrollBar': preload("handlers/ScrollBarHandler.tscn").instance(),
        'TextureRect': preload("handlers/NoOpHandler.tscn").instance()
   }

var _handler: AbstractControlHandler = null
var _last_mouse_pos: Vector2 = UNDEF
var _control_under_mouse: Control
var _mouse_pos: Vector2 = UNDEF

func _ready():
    ._ready()
    $Screen/GazeActivator.connect("pointing_at_changed", self, "_on_pointing_at_changed")
    $Screen/GazeActivator.connect("activated", self, "_on_activated")

func _on_pointing_at_changed(point):
    _last_mouse_pos = _mouse_pos
    if point:
        var p = to_local(point)
        var x = ((1 - rel_align_x)/2 + p.x/_screen_size.x) * _control.rect_size.x
        var y = ((1 + rel_align_y)/2 - p.y/_screen_size.y) * _control.rect_size.y
        _mouse_pos = Vector2(x, y)
        var control_under_mouse = NodeFinder.find_descentant($Projector, CONTROL_UNDER_MOUSE_FILTER, true)
        if control_under_mouse != _control_under_mouse:
            if _handler:
                _handler.control = null
            if control_under_mouse:
                _handler = _find_handler_for(control_under_mouse)
            else:
                _handler = null
            if _handler:
                _handler.control = control_under_mouse
        _control_under_mouse = control_under_mouse
    else:
        _mouse_pos = UNDEF
        _control_under_mouse = null
        if _handler:
            _handler.control = null
    Events.move_mouse_to($Projector, _last_mouse_pos, _mouse_pos)
    if _control_under_mouse and _handler:
        _handler.mouse_moved_to(_mouse_pos)

func _is_under_mouse(node: Node):
    return Events.is_under_mouse(_mouse_pos, node)

func _find_handler_for(control: Control) -> AbstractControlHandler:
    var classname = control.get_class()
    var handler: AbstractControlHandler
    while true:
        print("Classname: %s" % classname)
        if classname in HANDLERS.keys():
            handler = HANDLERS[classname]
        else:
            classname = ClassDB.get_parent_class(classname)
        if handler:
            print("Handler for %s: %s" % [classname, handler])
            break        
    handler.activator = $Screen/GazeActivator
    handler.viewport = $Projector
    return handler

func _on_activated():
    if _handler:
        _handler.activated()

func _on_UIPanel_visibility_changed():
    $Screen/CollisionShape.disabled = not visible
