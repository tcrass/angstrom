extends Node

# Based on https://github.com/Naata/godot-event-bus

enum PROCESSING_MODE {IDLE, PHYSICS}
export (int, "Idle", "Physics") var processing_mode = 0

const _subscriptions_by_channels_and_subscribers = {}
const _messages = []

var _messages_lock = Mutex.new()
var _subscriptions_lock = Mutex.new()

class Message:
    var subscriber = null
    var channel = null
    var method = null
    var data = null

func _ready():
    set_process(false)
    set_physics_process(false)
    if (processing_mode == PROCESSING_MODE.IDLE):
        set_process(true)
    elif(processing_mode == PROCESSING_MODE.PHYSICS):
        set_physics_process(true)

func _process(delta):
    _process_events();

func _physics_process(delta):
    _process_events()

func _process_events():
    while len(_messages) > 0:
        _messages_lock.lock()
        var message: Message = _messages.pop_front()
        _messages_lock.unlock()
        var subscriber = message.subscriber.get_ref()
        if subscriber:
            if _subscriptions_by_channels_and_subscribers[message.channel][message.subscriber]['wants_data']:
                subscriber.call(message.method, message.data)
            else:
                subscriber.call(message.method)
           

func publish(channel: String, data = null):
    var subscriptions = null
    
    _subscriptions_lock.lock()
    if _subscriptions_by_channels_and_subscribers.has(channel):
        subscriptions = _subscriptions_by_channels_and_subscribers[channel]
    _subscriptions_lock.unlock()
    
    if subscriptions:        
        for subscriber in subscriptions.keys():
            var message = Message.new()
            message.subscriber = subscriber
            message.channel = channel
            message.method = subscriptions[subscriber]['method']
            message.data = data
    
            _messages_lock.lock()
            _messages.push_back(message)
            _messages_lock.unlock()

func subscribe(subscriber, channel: String, method: String):
    var method_info = _get_method_info(subscriber, method)
    if method_info:
        var args_count = len(method_info['args'])
        if (args_count >= 0 and args_count <= 1):
            _subscriptions_lock.lock()
            if !_subscriptions_by_channels_and_subscribers.has(channel):
                _subscriptions_by_channels_and_subscribers[channel] = {}
            _subscriptions_lock.unlock()
            _subscriptions_by_channels_and_subscribers[channel][weakref(subscriber)] = {
                'method': method,
                'wants_data': args_count > 0
            }
        else:
            printerr("Subscriber method mus have 0 or 1 arguments, but %s.%s has %s!" % [subscriber, method, args_count])
    else:
        printerr("No such method: %s.%s" % [subscriber, method])


func _get_method_info(subscriber, method:String) -> Dictionary:
    var method_info
    for m in subscriber.get_method_list():
        if m['name'] == method:
            method_info = m
            break
    return method_info

func unsubscribe(subscriber, channel: String):
    # TODO: subscribers by channels are now weak refs!
    _subscriptions_by_channels_and_subscribers[channel].erase(subscriber)
