extends Node

func move_mouse_to(viewport: Viewport, from: Vector2, to: Vector2):
#    print("Moving mouse to %s" % to)
    viewport.input(craate_mouse_motion_event(from, to))

func craate_mouse_motion_event(from: Vector2, to: Vector2):
    var ev: InputEventMouseMotion = InputEventMouseMotion.new()
    ev.position = to
    ev.global_position = to
    ev.relative = to - from
    return ev

func click_at(viewport: Viewport, where: Vector2):
    press_mouse_button(viewport, where)
    release_mouse_button(viewport, where)
    
func press_mouse_button(viewport: Viewport,where: Vector2):
#    print("Pressing mouse button at %s" % where)
    viewport.input(create_mouse_button_event(where, true))
    
func release_mouse_button(viewport: Viewport,where: Vector2):
#    print("Releasing mouse button at %s" % where)
    viewport.input(create_mouse_button_event(where, false))
    
func create_mouse_button_event(where: Vector2, pressed: bool) -> InputEventMouseButton:
    var ev: InputEventMouseButton = InputEventMouseButton.new()
    ev.position = where
    ev.global_position = where
    ev.button_index = BUTTON_LEFT
    ev.pressed = pressed
    return ev

func is_under_mouse(where: Vector2, node : Object):
    if node.has_method("get_global_rect"):
        return node.get_global_rect().has_point(where);
    else:
        return false

func to_control_ccords(where: Vector2, control: Control):
    if where == Panel3D.UNDEF:
        return where
    else:
        return where - control.get_global_transform_with_canvas().get_origin()
 
