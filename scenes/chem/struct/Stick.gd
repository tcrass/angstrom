class_name Stick
extends Spatial

const STICK_RADIUS: float = 0.1

var atom1: Atom
var atom2: Atom
var length: float

func set_atoms(atom1: Atom, atom2: Atom):
    self.atom1 = atom1
    self.atom2 = atom2
    length = (atom1.translation - atom2.translation).length()

   
func _ready():
    _update_display()
        
func _update_display():
    $Stick1/Cap/MeshInstance.material_override = atom1.element_data['materials']['jmol']
    $Stick1/Rod/MeshInstance.material_override = atom1.element_data['materials']['jmol']
    $Stick2/Rod/MeshInstance.material_override = atom2.element_data['materials']['jmol']
    $Stick2/Cap/MeshInstance.material_override = atom2.element_data['materials']['jmol']

    $Stick1/Cap.scale = Vector3(STICK_RADIUS, STICK_RADIUS, STICK_RADIUS)
    $Stick1/Rod.scale = Vector3(STICK_RADIUS, STICK_RADIUS, length)
    $Stick1/Rod/MeshInstance.mesh.height = 0.5
    $Stick1/Rod/MeshInstance.translation = Vector3(0, 0, -0.25)

    $Stick2/Cap.scale = Vector3(STICK_RADIUS, STICK_RADIUS, STICK_RADIUS)
    $Stick2/Rod.scale = Vector3(STICK_RADIUS, STICK_RADIUS, length)
    $Stick2/Rod/MeshInstance.mesh.height = 0.5
    $Stick2/Rod/MeshInstance.translation = Vector3(0, 0, 0.25)
    $Stick2.translation = Vector3(0, 0, -length)
