extends Spatial
class_name Panel3D

const UNDEF: Vector2 = Vector2(-1, -1)

export var control: PackedScene setget _set_control
export var ui_size: Vector2 setget _set_ui_size, _get_ui_size
export var pixel_size: float = 0.005 setget _set_pixel_size
export var rel_align_x: float = 0 setget _set_rel_align_x
export var rel_align_y: float = 0 setget _set_rel_align_y
export var material: Material = create_default_material() setget _set_material
export var collision_layer: int setget _set_collision_layer, _get_collision_layer
export var collision_disabled: bool = false setget _set_collision_disabled, _get_collision_disabled

var _control: Control
var _screen_size: Vector2


func _set_control(value: PackedScene):
    if value != control:
        if $Projector.get_child_count() > 0:
            $Projector.get_child(0).queue_free()
        control = value
        if control:
            _control = value.instance() as Control
            _control.rect_position = Vector2.ZERO
            $Projector.add_child(_control)
        else:
            _control = null
        update_display()
    
func _set_ui_size(value: Vector2):
    $Projector.size = value
    if _control:
        _control.rect_size = value
    update_display()
    
func _set_pixel_size(value: float):
    pixel_size = value
    if $Screen/Sprite:
        $Screen/Sprite.pixel_size = value
    update_display()

func _set_rel_align_x(value: float):
    rel_align_x = value
    update_display()

func _set_rel_align_y(value: float):
    rel_align_y = value
    update_display()

func _set_material(value: Material):
    if value != material:
        if material:
            material = value
        else:
            material = create_default_material()
        update_display()

func _get_ui_size() -> Vector2:
    return $Projector.size

func _set_collision_layer(value: int):
    $Screen.collision_layer = value

func _get_collision_layer() -> int:
    return $Screen.collision_layer

func _set_collision_disabled(value: bool):
    $Screen/CollisionShape.disabled = value

func _get_collision_disabled() -> bool:
    return $Screen/CollisionShape.disabled
    
func get_control() -> Control:
    return _control
    
func get_screen_size() -> Vector2:
    return _screen_size

static func create_default_material() -> SpatialMaterial:
    var default_material = MeshSprite.create_default_material()
    default_material.flags_transparent = true
    default_material.flags_unshaded = true
    return default_material

func _ready():
    self.pixel_size = pixel_size
    update_display()

    $Screen/CollisionShape.disabled = not visible

func _process(delta: float):
    update_display()


func update_display():
    # TODO: Figure out when exactly layout calculations have to be performed,
    # in particular with auto-scaling controls like in classes like HUD.
    if _control:
        var control_size = _get_control_size()
        $Projector.size = control_size
        $Screen/Sprite.pixel_size = pixel_size
        $Screen/Sprite.material = material
        $Screen/Sprite.texture = $Projector.get_texture()
        
        _screen_size = Vector2(pixel_size * control_size.x, pixel_size * control_size.y)
        $Screen/CollisionShape.scale = Vector3(_screen_size.x, _screen_size.y, 1)
        $Screen.translation = Vector3(rel_align_x * _screen_size.x/2, rel_align_y * _screen_size.y/2, 0)


func _get_control_size() -> Vector2:
    if _control:
        return Vector2(int(_control.rect_size.x), int(_control.rect_size.y))
    else:
        return UNDEF

func _on_UIPanel_visibility_changed():
    $Screen/CollisionShape.disabled = not visible

func show():
    visible = true
    self.collision_disabled = false

func hide():
    visible = false
    self.collision_disabled = true
