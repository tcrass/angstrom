class_name DefaultReticlePainter
extends AbstractReticlePainter

export var thickness: float = 0.1
export var normal_radius: float = 0.1
export var normal_color: Color = Color("#ffffffff")
export var focused_duration: float = 0.3
export var focused_radius: float = 0.9
export var activation_inner_radius: float = 0.35
export var activation_outer_radius: float = 0.7
export var activation_color: Color = Color("#ffff00ff")

var _focused: float = 0
var _activation_progress: float = 0
var _activation_complete: bool = false

func _draw():
    var c: Vector2 = drawing_size/2
    var s: float = min(drawing_size.x, drawing_size.y) / 2.0
    var dr: float = focused_radius - normal_radius
    Painter.draw_ring(self, c, s*dr*_focused, s*(thickness + dr*_focused), normal_color)

    if _focused == 1:
        Painter.draw_ring(self, c, 0, s*normal_radius, normal_color)
        if  _activation_progress > 0:
            Painter.draw_ring(self, c, s*activation_inner_radius, s*activation_outer_radius, activation_color, 360*(1.25 - _activation_progress), 450)

func on_focused_changed(reticle: Reticle):
    $Tween.stop_all()
    var duration: float = focused_duration if focused_duration < reticle.effective_activation_pre_delay() else reticle.effective_activation_pre_delay()
    var target: float = 1.0 if reticle.focused else 0.0
    if duration > 0:
        ($Tween as Tween).interpolate_property(self, "_focused", _focused, target, duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)    
        ($Tween as Tween).start()
    else:
        _focused = target

func on_activation_progress_changed(reticle: Reticle):
    _activation_progress = reticle.activation_progress
    

func on_activation_complete_changed(reticle: Reticle):
    _activation_complete = reticle.activation_complete
