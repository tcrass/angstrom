class_name SelectPanel
extends PanelContainer

var choices: Array = [] setget _set_choices

onready var _buttons: Container = find_node('SelectButtonsContainer')
onready var _select_button: Button = find_node('SelectButton')
onready var _items: ItemList = find_node('ItemList')

var _selected_partition_index: int = 0
var _selected_item = null setget _set_selected_item

func _set_choices(value):
    choices = value
    _update_buttons()
    _select_partition(0)

func _set_selected_item(value):
    _selected_item = value
    _select_button.disabled = not value

func _update_buttons():
    var buttons = find_node('SelectButtonsContainer')
    for i in len(choices):
        var choice = choices[i]
        var button = buttons.get_child(i) as Button
        if choice['from'] == choice['to']:
            button.text = choice['from'].to_upper()
        else:
            button.text = ("%s-%s" % [choice['from'], choice['to']]).to_upper()

func _select_partition(n: int):
    self._selected_item = null
    _selected_partition_index = n
    _items.clear()
    for item in choices[n]['items']:
        _items.add_item(item['name'])

func _on_Button0_pressed():
    _select_partition(0)

func _on_Button1_pressed():
    _select_partition(1)

func _on_Button2_pressed():
    _select_partition(2)

func _on_Button3_pressed():
    _select_partition(3)

func _on_Button4_pressed():
    _select_partition(4)

func _on_Button5_pressed():
    _select_partition(5)

func _on_Button6_pressed():
    _select_partition(6)

func _on_Button7_pressed():
    _select_partition(7)

func _on_Button8_pressed():
    _select_partition(8)

func _on_Button9_pressed():
    _select_partition(9)

func _on_ItemList_item_selected(index):
    var items = choices[_selected_partition_index]['items']
    self._selected_item = items[index]

func _on_CloseButton_pressed():
    MessageBus.publish(Messages.HIDE_SEARCH)

func _on_SelectButton_pressed():
    MessageBus.publish(Messages.HIDE_SEARCH)
    print(_selected_item.keys())
    MessageBus.publish(Messages.SELECT_MOLECULE, _selected_item['data'])
