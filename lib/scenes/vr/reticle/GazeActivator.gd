extends Node
class_name GazeActivator

signal looked_at_changed(looked_at)
signal pointing_at_changed(point)
signal focusable_changed(focusable)
signal focused_changed(focused)
signal activatable_changed(activatable)
signal activated

export var focusable: bool = true setget set_focusable
export var activatable: bool = true setget set_activatable
export var activation_pre_delay_override: float = -1
export var activation_delay_override: float = -1

var looked_at: bool = false setget set_looked_at
var pointing_at = Vector3.ZERO setget set_pointing_at
var focused: bool = false setget set_focused

func set_looked_at(value: bool):
    if value != looked_at:
        looked_at = value
        emit_signal("looked_at_changed", looked_at)

func set_pointing_at(value): # Vector3
    if value != pointing_at:
        pointing_at = value
        emit_signal("pointing_at_changed", pointing_at)

func set_focusable(value: bool):
    if value != focusable:
        focusable = value
        emit_signal("focusable_changed", focusable)


func set_focused(value: bool):
    if value != focused:
        if !value || focusable:
            focused = value
            emit_signal("focused_changed", focused)

func set_activatable(value: bool):
    if value != activatable:
        activatable = value
        emit_signal("activatable_changed", activatable)
        

func activate():
    if activatable:
        emit_signal("activated")
      
