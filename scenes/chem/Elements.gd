extends Node

const ELEMENT_DATA_PATH = "res://data/element_data.json"

var by_symbol: Dictionary = {}
var by_atomic_number: Dictionary = {}

var _atom_scene = preload("res://scenes/chem/struct/Atom.tscn")
var _atom_material = preload("res://assets/materials/chem/atom_material.tres")
var _bond_scene = preload("res://scenes/chem/struct/Bond.tscn")

func _ready():
    var file = File.new()
    file.open(ELEMENT_DATA_PATH, File.READ)
    var data = parse_json(file.get_as_text())
    for element in data:
        var jmol_material = _atom_material.duplicate()
        jmol_material.albedo_color = Color(element['jmol_color'])
        element['materials'] = {
            'jmol': jmol_material
        }
        by_symbol[element['symbol']] = element
        by_atomic_number[int(element['atomic_number'])] = element

func create_atom(atom_data: Dictionary) -> Atom:
    var atom = create_atom_by_number(atom_data['number'])
    atom.data = atom_data
    return atom

func create_atom_by_number(atomic_number: int) -> Atom:
    var atom: Atom = _atom_scene.instance()
    atom.element_data = by_atomic_number[atomic_number]
    return atom

func create_bond(bond_data: Dictionary) -> Bond:
    var bond: Bond = _bond_scene.instance()
    bond.data = bond_data
    return bond   
