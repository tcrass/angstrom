extends StaticBody
class_name AbstractGazeActivatedButton

enum Mode {
    ACTION_BUTTON, 
    PUSH_BUTTON, 
    TOGGLE_BUTTON
}

signal pressed_changed(pressed, action)
signal clicked(action)

export (Mode) var mode = Mode.ACTION_BUTTON setget _set_mode
export var action: String
export var disabled: bool = false setget _set_disabled
export var click_duration: float = 0.4 setget _set_click_duration

export (AudioStream) var focus_gained_sound
export (AudioStream) var pressed_sound
export (AudioStream) var while_pressed_sound
export (AudioStream) var released_sound
export (AudioStream) var focus_lost_sound
export (float, 0, 1) var audio_volume = 1 setget _set_audio_volume


var radio_group: RadioGroup setget _set_radio_group

var focused: bool = false setget _set_focused
var pressed: bool = false setget _set_pressed

var _clicked: bool = false setget _set_clicked

func _set_mode(value):
    if value != mode:
        mode = value
        focused = false
        pressed = false
        _clicked = false
        if mode == Mode.PUSH_BUTTON:
            $GazeActivator.activation_pre_delay_override = 0.0
            $GazeActivator.activation_delay_override = 0.0
        else:
            $GazeActivator.activation_pre_delay_override = -1.0
            $GazeActivator.activation_delay_override = -1.0

func _set_radio_group(value):
    if value != radio_group:
        if radio_group:
            radio_group.unregister(self)
        radio_group = value
        if radio_group:
            radio_group.register(self)

func _set_disabled(value):
    if value != disabled:
        disabled = value
        if $GazeActivator:
            $GazeActivator.activatable = not disabled
            $GazeActivator.focusable = not disabled
        if disabled:
            self.focused = false
            
        update_display()
            

func _set_click_duration(value):
    $ClickTimer.wait_time = value

func _set_focused(value: bool):
    if value != focused:
        focused = value
        print("Focused: %s" % focused)
        if not focused:
            self._clicked = false
            $GazeActivator.activatable = not disabled

        _update_audio()
        if $FocusedAudioPlayer:
            $FocusedAudioPlayer.stop()
            if focused and focus_gained_sound:
                $FocusedAudioPlayer.stream = focus_gained_sound
                $FocusedAudioPlayer.play()
            elif focus_lost_sound:
                $FocusedAudioPlayer.stream = focus_lost_sound
                $FocusedAudioPlayer.play()
                
        update_display()
        
func _set_pressed(value: bool):
    if value != pressed:
        pressed = value
        print("Pressed: %s" % pressed)
        $GazeActivator.focusable = not disabled and (not radio_group or not pressed)

        _update_audio()
        if $PressedAudioPlayer:
            $PressedAudioPlayer.stop()
            if pressed and pressed_sound:
                $PressedAudioPlayer.stream = pressed_sound
                $PressedAudioPlayer.play()
            elif released_sound:
                $PressedAudioPlayer.stream = released_sound
                $PressedAudioPlayer.play()
        if $WhilePressedAudioPlayer:
            $WhilePressedAudioPlayer.stop()
            if pressed and while_pressed_sound:
                $WhilePressedAudioPlayerr.stream = while_pressed_sound
                $WhilePressedAudioPlayer.play()
        
        update_display()
        emit_signal("pressed_changed", pressed, action)


func _set_clicked(value):
    if value != _clicked:
        _clicked = value
        print("Clicked: %s" % _clicked)
        if _clicked:
            self.pressed = true
            $GazeActivator.activatable = false
            $ClickTimer.start()
            emit_signal("clicked", action)
        else:
            self.pressed = false

func _set_audio_volume(value: float):
    if value != audio_volume:
        audio_volume = value
        _update_audio()

func _ready():
    $GazeActivator.connect("focused_changed", self, "_on_GazeActivator_focused_changed")
    $GazeActivator.connect("activated", self, "_on_GazeActivator_activated")
    _update_audio()
    update_display()
    
func update_display():
    pass

func _update_audio():
    var target_db: float = -100 if audio_volume == 0 else 6 + 36*(audio_volume - 1)
    if $FocusedAudioPlayer:
        $FocusedAudioPlayer.unit_db = target_db
    if $PressedAudioPlayer:
        $PressedAudioPlayer.unit_db = target_db
    if $WhilePressedAudioPlayer:
        $WhilePressedAudioPlayer.unit_db = target_db
    

func _on_GazeActivator_activatable_changed(activatable):
    pass
#    if activatable:
#        enabled = true
#    else:
#        enabled = false

func _on_GazeActivator_focused_changed(value: bool):
    print("Focussed changed: %s" % value)
    self.focused = value
    match mode:
        Mode.ACTION_BUTTON:
            pass
        Mode.PUSH_BUTTON:
            self.pressed = not disabled and focused
        Mode.TOGGLE_BUTTON:
            pass
                
        
func _on_GazeActivator_activated():
    print("Activated!")
    match mode:
        Mode.ACTION_BUTTON:
            self._clicked = true
        Mode.PUSH_BUTTON:
            pass
        Mode.TOGGLE_BUTTON:
            if not radio_group || not self.pressed:
                self.pressed = not pressed
            if self.pressed and radio_group:
                radio_group.selected = self
                
func _on_ClickTimer_timeout():
    self._clicked = false
