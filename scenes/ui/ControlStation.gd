class_name ControlStation
extends Spatial

var _display_mode_group = RadioGroup.new()

var _rot_controls_showing: bool = true


func _ready():
    $ButtonPanel/SpaceFillingButton.radio_group = _display_mode_group
    $ButtonPanel/BallAndStickButton.radio_group = _display_mode_group
    $ButtonPanel/SticksOnlyButton.radio_group = _display_mode_group
    _display_mode_group.connect("selected_changed", self, "_on_displayModeButtonSelected")
    
    MessageBus.subscribe(self, Messages.STRUCTURE_CHANGED, '_on_structureChanged')
    MessageBus.subscribe(self, Messages.SELECT_DISPLAY_MODE, '_on_selectDisplayMode')
    
    _update_display()
    
func _on_PrevButton_clicked(action):
    MessageBus.publish(Messages.SELECT_PREV)

func _on_RandomButton_clicked(action):
    MessageBus.publish(Messages.SELECT_RANDOM)

func _on_NextButton_clicked(action):
    MessageBus.publish(Messages.SELECT_NEXT)

func _on_ConformerButton_clicked(action):
    MessageBus.publish(Messages.SELECT_CONFORMER)

func _on_SettingsButton_clicked(action):
    MessageBus.publish(Messages.SHOW_SETTINGS)

func _on_RotControlsButton_pressed_changed(pressed, action):
    _rot_controls_showing = pressed
    _update_display()
    if pressed:
        MessageBus.publish(Messages.SHOW_ROTATION)
    else:
        MessageBus.publish(Messages.HIDE_ROTATION)

func _on_SearchButton_clicked(action):
    MessageBus.publish(Messages.SHOW_SEARCH)

func _on_displayModeButtonSelected(button):
    match button.action:
        "SPACE_FILLING":
            MessageBus.publish(Messages.SELECT_DISPLAY_MODE, Molecule.DisplayMode.SPACE_FILLING)
        "BALL_AND_STICK":
            MessageBus.publish(Messages.SELECT_DISPLAY_MODE, Molecule.DisplayMode.BALL_AND_STICK)
        "STICKS_ONLY":
            MessageBus.publish(Messages.SELECT_DISPLAY_MODE, Molecule.DisplayMode.STICKS_ONLY)
            
    
func _update_display():
    $ButtonPanel/RotControlsButton.pressed = _rot_controls_showing


func _on_structureChanged(structure: Structure):
    $ButtonPanel/ConformerButton.disabled = structure.data['conformers'].size() <= 1
  

func _on_selectDisplayMode(mode):
    match mode:
        Molecule.DisplayMode.SPACE_FILLING:
            _display_mode_group.selected = $ButtonPanel/SpaceFillingButton
        Molecule.DisplayMode.BALL_AND_STICK:
            _display_mode_group.selected = $ButtonPanel/BallAndStickButton
        Molecule.DisplayMode.STICKS_ONLY:
            _display_mode_group.selected = $ButtonPanel/SticksOnlyButton


