extends Node

const ANGULAR_STEPS = 72

static func draw_ring(
        o: CanvasItem, 
        center: Vector2, 
        r_inner: float,
        r_outer: float,
        color: Color, 
        from_angle: float = 0, 
        to_angle: float = 360, 
        steps: int = ANGULAR_STEPS
    ):
    var d_angle: float = to_angle - from_angle
    var n: int = int(ANGULAR_STEPS * d_angle/360)
    var points: PoolVector2Array = PoolVector2Array()
    var colors: PoolColorArray = PoolColorArray([color])
    
    if n > 0:
        for i in range(n + 1):
            var angle: float = deg2rad(from_angle + i * d_angle / n)
            points.append(center + Vector2(cos(angle), sin(angle)) * r_outer)
        for i in range(n + 1, 0, -1):
            var angle: float = deg2rad(from_angle + i * d_angle / n)
            points.append(center + Vector2(cos(angle), sin(angle)) * r_inner)

        o.draw_polygon(points, colors, Const2.EMPTY_ARRAY, null, null, true)
