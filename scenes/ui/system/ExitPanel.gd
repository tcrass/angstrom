class_name ExitPanel
extends PanelContainer

func _on_ExitButton_pressed():
    MessageBus.publish(Messages.EXIT_APPLICATION)
