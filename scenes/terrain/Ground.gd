extends StaticBody

export var create_mountains: bool = true

var mountains_height : float = 30
var mountains_dist : float = 0.5
var mountains_slope : float = 10

func _ready():
    if create_mountains:
        var boundingBox = $Mesh.get_aabb()
        
        var st = SurfaceTool.new()
        st.create_from($Mesh.mesh, 0)
        var array = st.commit()
        
        var mdt = MeshDataTool.new()
        mdt.create_from_surface(array, 0)
        
        var noise = create_noise()    
        for i in range(mdt.get_vertex_count()):
            var v : Vector3 = mdt.get_vertex(i)
            var pos = Vector2(v.x / boundingBox.size.x, v.z / boundingBox.size.z)
            v.y = 0.5 * mountains_height * sigm(pos.length(), mountains_dist, mountains_slope) * (noise.get_noise_2d(pos.x, pos.y) + 1)
            mdt.set_vertex(i, v)
            
        for s in range(array.get_surface_count()):
            array.surface_remove(s)
        
        mdt.commit_to_surface(array)
        
        st.create_from(array, 0)
        st.generate_normals()
        
        $Mesh.mesh = st.commit()
        
        var collision_shape = ConcavePolygonShape.new()
        collision_shape.set_faces($Mesh.mesh.get_faces())
        $CollisionShape.set_shape(collision_shape)

   
        
func create_noise() -> OpenSimplexNoise:
    var noise = OpenSimplexNoise.new()
    noise.seed = randi()
    noise.octaves = 5
    noise.period = 0.1
    noise.persistence = 0.4
    return noise
        
func sigm(x : float, dist : float, slope: float) -> float:
    return (1 / (1 + exp(slope*(dist - x))) - 1 / (1 + exp(slope*dist))) / (1 / (1 + exp(slope*(dist - 1))) - 1 / (1 + exp(slope*dist)))
