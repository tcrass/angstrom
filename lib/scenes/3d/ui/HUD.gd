class_name HUD
extends Spatial

export var control: PackedScene setget _set_control, _get_control
export var pixel_size: float = 0.005 setget _set_pixel_size
export var size: Vector2 = Vector2.ONE setget _set_size
export var distance: float = 3 setget _set_distance
export var material: Material = create_default_material() setget _set_material

func _set_control(value: PackedScene):
    if value != $Panel3D.control:
        $Panel3D.control = value
        update_display()

func _get_control() -> PackedScene:
    return $Panel3D.control

func _set_pixel_size(value: float):
    if value != pixel_size:
        pixel_size = value
        update_display()
   
func _set_size(value: Vector2):
    if value != size:
        size = value
        update_display()

func _set_distance(value: float):
    if distance != value:
        distance = value
        update_display()

func _set_material(value: Material):
    if value != material:
        if material:
            material = value.duplicate()
        else:
            material = create_default_material()
        update_display()
        
func get_control() -> Control:
    return $Panel3D.get_control()

static func create_default_material() -> SpatialMaterial:
    var default_material = Panel3D.create_default_material()
    default_material.flags_do_not_receive_shadows = true
    default_material.flags_unshaded = true
    default_material.flags_no_depth_test = true
    default_material.render_priority = 9
    return default_material

func _ready():
    $Panel3D.collision_layer = 0
    $Panel3D.collision_disabled = true

func _process(delta: float):
    update_display()
    
    
func update_display():
    $Panel3D.material = material
    if get_viewport():
        var camera = get_viewport().get_camera()
        if camera and camera.current:
            var is_vr = camera is ARVRCamera

            global_transform = camera.global_transform
            $Panel3D.translation = distance * Vector3.FORWARD

            
            var viewport_size = get_viewport().size
            if is_vr:
                viewport_size.x /= 2
            
            var viewport_dist: float
            if camera.keep_aspect == Camera.KEEP_HEIGHT:
                viewport_dist = (viewport_size.y/2) / tan(deg2rad(camera.fov/2))
            else:
                viewport_dist = (viewport_size.x/2) / tan(deg2rad(camera.fov/2))
            
            var hud_dist = ($Panel3D.global_transform.origin - camera.global_transform.origin).length()
            var hud_size = Vector2(hud_dist * viewport_size.x/viewport_dist, hud_dist * viewport_size.y/viewport_dist)

            $Panel3D.pixel_size = pixel_size
            $Panel3D.ui_size = Vector2(int(hud_size.x/pixel_size), int(hud_size.y/pixel_size))
