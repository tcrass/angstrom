class_name ItemListHandler
extends AbstractControlHandler

var _item_index: int = -1

func control_entered():
    .control_entered()
    _item_index = -1

func _update_activator():
    var item_index = _get_item_index()
    if item_index != _item_index:
        _enable_activator(false)
    _item_index = item_index
    print("Item index: %s" % _item_index)
    if _mouse_pos != Panel3D.UNDEF:
        _enable_activator(_item_index >= 0 and not _is_control_disabled())    
    else:
        _enable_activator(false)    

func _get_item_index() -> int:
    var mouse_pos = Events.to_control_ccords(_mouse_pos, control)
    return control.get_item_at_position(mouse_pos, true)

func control_exited():
    .control_exited()
    _item_index = -1
