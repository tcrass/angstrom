class_name ControlStationButton
extends SpriteButton

func _ready():
    ._ready()
    collision_layer = Constants.CL_WIDGETS
    MessageBus.subscribe(self, Messages.VOLUME_CHANGED, '_on_volumeChanged')

func _on_volumeChanged(volume: float):
    audio_volume = volume
