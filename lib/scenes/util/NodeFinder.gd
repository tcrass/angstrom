extends Node

var CURRENT_CAMERA_FILTER = funcref(self, "_is_current_camera")

func find_descentant(root, filter, find_last = false):
    var result = null
    
    var current = root;
    var nodes = [current]
        
    while (result == null || find_last) && nodes.size() > 0:
        var node = nodes.pop_front()
        if filter.call_func(node):
            result = node
        if !result || find_last:
            for child in node.get_children():
                nodes.append(child)
            
    return result

func find_current_camera(root):
    var camera = find_descentant(root, CURRENT_CAMERA_FILTER)
    return camera
    
func _is_current_camera(node):
    return (node is Camera) && node.current

func find_ancestor(root, filter):
    var result = null
    
    var current = root
    
    while result == null && current != null:
        if filter.call_func(current):
            result = current
        else:
            current = current.get_parent()
    
    return result
    
