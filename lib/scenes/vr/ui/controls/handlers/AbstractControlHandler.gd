class_name AbstractControlHandler
extends Node


var viewport: Viewport
var activator: GazeActivator

var control: Control setget _set_control

var _original_activation_delay: int

var _mouse_pos: Vector2 = Panel3D.UNDEF
var _mouse_button_pressed: bool = false

func _set_control(value: Control):
    if value != control:
        control = value
        if control:
            control_entered()
        else:
            control_exited()

func _process(delta: float):
    _update_activator()

func control_entered():
    _original_activation_delay = activator.activation_delay_override
    if control.has_meta(VRControls.ACTIVATION_DELAY):
        activator.activation_delay_override = control.get_meta(VRControls.ACTIVATION_DELAY)
    
func mouse_moved_to(where: Vector2):
    _mouse_pos = where
    _update_activator()

func activated():
    if control.get_meta(VRControls.PRESS_ON_ACTIVATION):
        _presse_mouse_button()
    else:
        _click()    

func control_exited():
    _release_mouse_button()
    _mouse_pos = Panel3D.UNDEF
    _enable_activator(false)
    activator.activation_delay_override = _original_activation_delay

func _click():
    _presse_mouse_button()
    _release_mouse_button()

func _presse_mouse_button():
    if not _mouse_button_pressed:
        print("Pressing...")
        Events.press_mouse_button(viewport, _mouse_pos)
        _mouse_button_pressed = true
        
func _release_mouse_button():
    if _mouse_button_pressed:
        print("Releasing...")
        Events.release_mouse_button(viewport, _mouse_pos)
        _mouse_button_pressed = false
        
func _update_activator():
    _enable_activator(_mouse_pos != Panel3D.UNDEF and not _is_control_disabled())
            
func _enable_activator(enabled: bool):
    activator.focusable = enabled
    activator.activatable = enabled
    
func _is_control_disabled():
    if 'disabled' in control:
        return control.disabled
    else:
        return false
