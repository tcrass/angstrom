#!/usr/bin/env python3
#

import json
import re
import shutil
import sys
from time import sleep
import urllib
from urllib.parse import quote as url_quote
import urllib.request as request

# AROMATIC = 1 << 1
# WEDGE = 1 << 2
# HASH = 1 << 3
# RING = 1 << 4
# CLOSURE = 1 << 10
# WEDGE_OR_HASH = 1 << 11
# CIS_OR_TRANS = 1 << 12

# HYB_TYPES = ['none', 'sp', 'sp2', 'sp3', 'sq. planar', 'trig. bipyr.', 'octahedral']

COMPUND_NAMES_FILE = "test-compound_names.txt"
MOLECULE_URL_PATTERN = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/{0}/json"
SYNONYMS_URL_PATTERN = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{0}/synonyms/json"
CONFORMERS_URL_PATTERN = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{0}/conformers/json"
CONFORMER_URL_PATTERN = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/conformers/{0}/json"
QUERY_DELAY_MS = 200
MOLECULES_JSON = "molecules.json"
SUCCESSFUL_TXT = "successful-molecules.txt"
FAILED_TXT = "failed-molecules.txt"

VALID_SYNONYM_PATTERNS = [
    re.compile('[a-z]')
]


def keep_synonym(syn):
    keep = True
    for re in VALID_SYNONYM_PATTERNS:
        keep = keep and re.search(syn)
        if not keep:
            break
    return keep

data = {}
successful = []
failed = []
with open(COMPUND_NAMES_FILE, 'r') as compound_list:
    for i, line in enumerate(compound_list):
        compound_name = line.strip()
        if compound_name and not compound_name.startswith('#'):
            print("Processing compound {0}...".format(compound_name))
            try:
                query_name = url_quote(compound_name)
                url = MOLECULE_URL_PATTERN.format(query_name)
                response = request.urlopen(url)
                jmols = json.load(response)
                for jmol in jmols['PC_Compounds']:
                    mol = {
                        'ids': {},
                        'names': {},
                        'atoms': {},
                        'bonds': [],
                        'conformers': {},
                    }

                    mol['ids']['cid'] = jmol['id']['id']['cid']

                    for prop in jmol['props']:
                        if prop['urn']['label'] == 'IUPAC Name' and prop['urn']['name'] == 'Preferred':
                            mol['names']['iupac'] = prop['value']['sval']
                        if prop['urn']['label'] == 'Molecular Formula':
                            mol['formula'] = prop['value']['sval']
                        if prop['urn']['label'] == 'Molecular Weight':
                            mol['mw'] = prop['value']['fval']

                    mol['charge'] = jmol['charge']

                    aids = jmol['atoms']['aid']
                    elements = jmol['atoms']['element']
                    for j in range(len(aids)):
                        mol['atoms'][str(aids[j])] = {
                                'number': elements[j],
                                'valence': 0
                        }

                    if 'bonds' in jmol:
                        orders = jmol['bonds']['order']
                        aids1 = jmol['bonds']['aid1']
                        aids2 = jmol['bonds']['aid2']
                        for j in range(len(orders)):
                            mol['bonds'].append({
                                    'order': orders[j],
                                    'id1': str(aids1[j]),
                                    'id2': str(aids2[j])
                            })
                            mol['atoms'][str(aids1[j])]['valence'] += 1
                            mol['atoms'][str(aids2[j])]['valence'] += 1

                    url = SYNONYMS_URL_PATTERN.format(mol['ids']['cid'])
                    response = request.urlopen(url)
                    jsyns = json.load(response)
                    syns = jsyns['InformationList']['Information'][0]['Synonym']
                    mol['names']['synonyms'] = [s for s in syns if keep_synonym(s)][0:5]

                    url = CONFORMERS_URL_PATTERN.format(mol['ids']['cid'])
                    response = request.urlopen(url)
                    jconf_ids = json.load(response)
                    conf_ids = jconf_ids['InformationList']['Information'][0]['ConformerID']
                    for conf_id in conf_ids:
                        url = CONFORMER_URL_PATTERN.format(conf_id)
                        response = request.urlopen(url)
                        jconfs = json.load(response)
                        for jconf in jconfs['PC_Compounds']:
                            jcoords = jconf['coords'][0]

                            conf = {
                                'atoms': {}
                            }
                            
                            aids = jcoords['aid']
                            for j in range(len(aids)):
                                atom = {
                                    'x': jcoords['conformers'][0]['x'][j],
                                    'y': jcoords['conformers'][0]['y'][j],
                                    'z': jcoords['conformers'][0]['z'][j],
                                }
                                conf['atoms'][aids[j]] = atom

                        mol['conformers'][conf_id] = conf

                data[mol['ids']['cid']] = mol
                successful.append(compound_name)

            except urllib.error.HTTPError as ex:
                print("- Download error for {0}: {1}\n    URL: {2}".format(compound_name, ex, url))
                failed.append(compound_name)

            sleep(QUERY_DELAY_MS/1000)
     

print("{0} molecules successfully downloaded, {1} failed.".format(len(successful), len(failed)))

with open(MOLECULES_JSON, 'w') as molecules_json:
    json.dump(data, molecules_json, indent=2)

with open(SUCCESSFUL_TXT, 'w') as successful_txt:
    for compound_name in successful:
        print(compound_name, successful_txt)

with open(FAILED_TXT, 'w') as failed_txt:
    for compound_name in failed:
        print(compound_name, failed_txt)
