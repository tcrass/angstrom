class_name StructureHUD
extends HUD

onready var panel: StructureHUDPanel = self.get_control() as StructureHUDPanel

func _ready():
    MessageBus.subscribe(self, Messages.INFO_CHANGED, '_on_infoChanged')
    MessageBus.subscribe(self, Messages.DETAILS_INFO_CHANGED, '_on_detailsInfoChanged')

func _on_infoChanged(info):
    if info['names']['iupac'] == info['names']['synonyms'][0]:
        panel.top_left = "%s" % [info['names']['iupac']]
    else:
        panel.top_left = "%s\n(%s)" % [info['names']['iupac'], info['names']['synonyms'][0]]
    panel.top_right = "Mw = %s\nCharge = %s" % [info['mw'], info['charge']]

func _on_detailsInfoChanged(info):
    panel.bottom_left = info[0] if info else "\n"
    panel.bottom_right = info[1] if info else ""
