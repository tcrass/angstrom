class_name Molecule
extends Structure

enum DisplayMode {
    SPACE_FILLING, 
    BALL_AND_STICK, 
    STICKS_ONLY
}

var atoms: Dictionary = {}
var bonds: Array = []

func _init():
    preferred_scale = 0.5

func _ready():
    MessageBus.subscribe(self, Messages.ATOM_CHANGED, '_on_atomChanged')
    MessageBus.subscribe(self, Messages.SELECT_DISPLAY_MODE, "_on_selectDisplayMode")

func get_extent() -> AABB:
    var new: bool = true
    var a: Vector3
    var b: Vector3
    for child in $Atoms.get_children():
        var pos: Vector3 = (child as Atom).translation
        if new:
            new = false
            a = pos
            b = pos
        else:
            if pos.x < a.x:
                a.x = pos.x
            if pos.y < a.y:
                a.y = pos.y
            if pos.z < a.z:
                a.z = pos.z
            if pos.x > b.x:
                b.x = pos.x
            if pos.y > b.y:
                b.y = pos.y
            if pos.z > b.z:
                b.z = pos.z
    var e: AABB = AABB(a, b - a)
    return e

func _on_atomChanged(atom):
    var info: Array
    if atom:
        info = [
            "%s (Z = %s)\n%s" % [
                atom.element_data['symbol'], 
                atom.element_data['atomic_number'],
                atom.element_data['name']
            ],
            "VdV Rad. = %s Å\nValence = %s" % [
                atom.element_data['vdv_radius'],
                atom.data['valence']
            ]
        ]
    MessageBus.publish(Messages.DETAILS_INFO_CHANGED, info)

func _on_selectDisplayMode(mode):
    set_display_mode(mode, true)

func add_atom(atom: Atom):
    atoms[atom.data['id']] = atom;
    $Atoms.add_child(atom)

func add_bond(bond: Bond):
    bonds.append(bond)
    $Bonds.add_child(bond)

func set_display_mode(mode, tween: bool):
    match mode:
        DisplayMode.SPACE_FILLING:
            set_display_mode_space_filling(tween)
        DisplayMode.BALL_AND_STICK:
            set_display_mode_ball_and_sticks(tween)
        DisplayMode.STICKS_ONLY:
            set_display_mode_sticks_only(tween)
            
func set_display_mode_space_filling(tween: bool):
    for id in atoms:
        atoms[id].apply_vdv_radius(tween)
    
func set_display_mode_ball_and_sticks(tween: bool):
    for id in atoms:
        atoms[id].set_radius(Atom.BALL_RADIUS, tween)
            
func set_display_mode_sticks_only(tween: bool):
    for id in atoms:
        atoms[id].set_radius(0, tween)
