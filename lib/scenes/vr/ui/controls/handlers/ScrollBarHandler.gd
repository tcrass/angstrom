class_name ScrollBarHandler
extends AbstractControlHandler

var _in_grabber: bool

func _update_activator():
    var in_grabber: bool
    if _mouse_pos != Panel3D.UNDEF:
        var mouse_pos: Vector2 = Events.to_control_ccords(_mouse_pos, control)
        var grabber_start = control.ratio
        var grabber_end = control.ratio + control.page/(control.max_value - control.min_value)
        var target
        if control is HScrollBar:
            target = mouse_pos.x / control.rect_size.x
        else:
            target = mouse_pos.y / control.rect_size.y
        in_grabber = target >= grabber_start and target <= grabber_end
        if in_grabber != _in_grabber:
            _enable_activator(false)
        _in_grabber = in_grabber
        _enable_activator(not _is_control_disabled())
    else:
        _enable_activator(false)


func activated():
    if _in_grabber:
        _presse_mouse_button()
    else:
        _click()

func control_exited():
    _release_mouse_button()
    .control_exited()
