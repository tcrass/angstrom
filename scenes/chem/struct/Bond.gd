class_name Bond
extends Spatial

const STICK_RADIUS: float = 0.1

var _stick_scene = preload("res://scenes/chem/struct/Stick.tscn")

var data: Dictionary setget _set_data
var atom1: Atom
var atom2: Atom
var vec: Vector3

func _set_data(value):
    data = value
    
    atom1 = data['atom1']
    atom1.data['bonds'].append(self)
    atom1.data['bonds1'].append(self)

    atom2 = data['atom2']
    atom2.data['bonds'].append(self)
    atom2.data['bonds2'].append(self)
    
    vec = atom2.translation - atom1.translation
    look_at_from_position(atom1.translation, atom2.translation, Vector3.UP)


func create_sticks():
    var dir1: Vector3 = _calc_offset_direction(atom1)
    var dir2: Vector3 = _calc_offset_direction(atom2)
    var dir: Vector3
    
    if dir1.length() == 0 || dir2.length() == 0:
        dir = dir1 if dir1.length() != 0 else dir2
    else:
        if (dir1 + dir2).length() > (dir1 - dir2).length():
            dir = (dir1 + dir2).normalized()
        else:
            dir = (dir1 - dir2).normalized()
    
    if dir.length() == 0:
        dir = Utils3D.any_perpendicular(vec).normalized()

    var o: float = 1.5 * (data['order'] - 1)
    #print("  dir: %s, o: %s" % [dir, o])

    for i in range(0, data['order']):
        var f: float = o - 3*i
        var offset = f * Stick.STICK_RADIUS
        var transl = offset * dir
        #print("    f: %s, offset: %s, transl: %s" % [f, offset, transl])
        var stick: Stick = _stick_scene.instance()
        stick.set_atoms(atom1, atom2)
        stick.translation = transl
        add_child(stick)


func _calc_offset_direction(atom: Atom) -> Vector3:
    var b: Vector3 = vec.normalized()
    var p: Plane = Plane(b, 0)
    var u: Vector3 = Utils3D.any_perpendicular(b).normalized()
    var v: Vector3 = b.cross(u).normalized()
    
    var su: float = 0
    var sv: float = 0
    var others = []
    for other in atom.data['bonds']:
        if other != self:
            others.append(other)
            var o = other.vec
            var proj: Vector3 = p.project(o)
            var pu = proj.dot(u)
            var pv = proj.dot(v)
            if (pu > 0):
                su += pu
                sv += pv
            else:
                su -= pu
                sv -= pv

    var dir = (su*u + sv*v).normalized()
    var basis: Basis = transform.inverse().basis
    var t: Transform = Transform(basis, Vector3.ZERO)
    return t.xform(dir)
