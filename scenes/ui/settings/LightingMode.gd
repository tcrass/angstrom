class_name LightingMode
extends Node

const SUNLIGHT = "sunlight"
const SUNSET = "sunset"
const MOONLIGHT = "moonlight"
