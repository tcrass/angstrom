extends Node
class_name Messages

const EXIT_APPLICATION = "exit"

const STRUCTURE_CHANGED = "structure_changed"
const ATOM_CHANGED = "atom_changed"

const INFO_CHANGED = "info_changed"
const DETAILS_INFO_CHANGED = "details_info_changed"

const SHOW_ROTATION = "show_rotation"
const HIDE_ROTATION = "hide_rotation"
const ROTATE_UP = "rotate_up"
const ROTATE_LEFT = "rotate_left"
const AUTO_ROTATE_LEFT = "auto_rotate_left"
const ROTATE_RIGHT = "rotate_right"
const AUTO_ROTATE_RIGHT = "auto_rotate_right"
const ROTATE_DOWN = "rotate_down"

const SHOW_SETTINGS = "show_settings"
const HIDE_SETTINGS = "hide_settings"

const LIGHTING_MODE_CHANGED = "lighting_changed"
const VOLUME_CHANGED = "volume_changed"
const MUSIC_VOLUME_CHANGED = "music_volume_changed"

const SHOW_SEARCH = "show_search"
const HIDE_SEARCH = "hide_search"

const SELECT_NEXT = "select_prev"
const SELECT_RANDOM = "select_random"
const SELECT_PREV = "select_next"
const SELECT_MOLECULE = "select_molecule"
const SELECT_CONFORMER = "select_conformer"

const SELECT_DISPLAY_MODE = "select_display_mode"
