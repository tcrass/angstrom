class_name DownPanel
extends PanelContainer

func _on_DownButton_button_down():
    MessageBus.publish(Messages.ROTATE_DOWN, true)

func _on_DownButton_button_up():
    MessageBus.publish(Messages.ROTATE_DOWN, false)
