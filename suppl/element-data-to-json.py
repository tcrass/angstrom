#!/usr/bin/env python3
#

import csv
import json

CSV = "element_data.csv"
JSON = "element_data.json"


data = []
with open(CSV) as infile:
    reader = csv.DictReader(infile)
    for row in reader:
        data.append({
            'atomic_number': int(row['atomic_number']),
            'symbol': row['symbol'],
            'name': row['name'],
            'vdv_radius': float(row['vdv_radius']),
            'jmol_color': "#{0}".format(row['jmol_color']),
        })

with open(JSON, 'w') as outfile:
    json.dump(data, outfile, indent=2)
   