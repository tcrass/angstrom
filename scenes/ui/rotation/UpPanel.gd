class_name UpPanel
extends PanelContainer

func _on_UpButton_button_down():
    MessageBus.publish(Messages.ROTATE_UP, true)

func _on_UpButton_button_up():
    MessageBus.publish(Messages.ROTATE_UP, false)
  
