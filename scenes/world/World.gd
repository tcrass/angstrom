extends Spatial    

const MOBILE_OS = ['Android', 'iOS']

var _sunlight_env = preload('res://assets/environments/sunlight.tres')
var _sunset_env = preload('res://assets/environments/sunset.tres')
var _moonlight_env = preload('res://assets/environments/moonlight.tres')

export(bool) var is_vr = false

onready var _rotation_controls_showing: bool = $UI/RotationControls.visible

func _ready():
    var vr: bool = is_vr or MOBILE_OS.has(OS.get_name())
    if vr or MOBILE_OS.has(OS.get_name()): 
        var arvr_interface : MobileVRInterface = ARVRServer.find_interface("Native mobile")
        if arvr_interface and arvr_interface.initialize():
            get_viewport().arvr = true
            $Player/ARVRCamera.make_current()
    else:
        $Player/Camera.make_current()
        
    # --- Component initialization ---
    
    $UI/ExitPanel.collision_layer = Constants.CL_ALL
    $UI/ExitPanel.material.flags_no_depth_test = true
    $UI/ExitPanel.material.render_priority = 7
    
    $UI/SettingsPanel.collision_layer = Constants.CL_PANELS
    $UI/SettingsPanel.hide()    
    
    $UI/SearchPanel.collision_layer = Constants.CL_PANELS
    $UI/SearchPanel.hide()    
    $UI/SearchPanel.get_control().choices = $Structures/Molecules.molecules_partitioned

    # --- Wiring ---

    MessageBus.subscribe(self, Messages.EXIT_APPLICATION, '_on_exitApplication')

    MessageBus.subscribe(self, Messages.SHOW_SETTINGS, '_on_showSettings')
    MessageBus.subscribe(self, Messages.HIDE_SETTINGS, '_on_hideSettings')

    MessageBus.subscribe(self, Messages.SHOW_ROTATION, '_on_showRotation')
    MessageBus.subscribe(self, Messages.HIDE_ROTATION, '_on_hideRotation')
    
    MessageBus.subscribe(self, Messages.SHOW_SEARCH, '_on_showSearch')
    MessageBus.subscribe(self, Messages.HIDE_SEARCH, '_on_hideSearch')
    
    MessageBus.subscribe(self, Messages.LIGHTING_MODE_CHANGED, '_on_lightingModeChanged')
    
    # --- State initialization ---

    MessageBus.publish(Messages.LIGHTING_MODE_CHANGED, LightingMode.SUNLIGHT)
    MessageBus.publish(Messages.AUTO_ROTATE_RIGHT, true)
    MessageBus.publish(Messages.SHOW_ROTATION)
    MessageBus.publish(Messages.SELECT_DISPLAY_MODE, Molecule.DisplayMode.SPACE_FILLING)

    MessageBus.publish(Messages.MUSIC_VOLUME_CHANGED, 100 if vr else 0)
    MessageBus.publish(Messages.VOLUME_CHANGED, 100)

    $Structures.select_random()
    #$Structures.select_by_pubchem_id(444972) # fumaric acid
    #$Structures.select_by_pubchem_id(1182) # valine
    #$Structures.select_by_pubchem_id(6057) # tyrosine
    #$Structures.select_by_pubchem_id(3760025) # ethinylaniline


func _on_exitApplication():
    get_tree().quit()

func _on_showSettings():
    $UI/SearchPanel.hide()
    $UI/SettingsPanel.show()
    _update_display()
    
func _on_hideSettings():
    $UI/SettingsPanel.hide()
    _update_display()

func _on_showRotation():
    _rotation_controls_showing = true
    _update_display()
    
func _on_hideRotation():
    _rotation_controls_showing = false
    _update_display()

func _on_showSearch():
    $UI/SettingsPanel.hide()
    $UI/SearchPanel.show()
    _update_display()

func _on_hideSearch():
    $UI/SearchPanel.hide()
    _update_display()

func _update_display():
    var panel_showing: bool = $UI/SearchPanel.visible or $UI/SettingsPanel.visible
    $Reticle.collision_mask = Constants.CL_CONTROLS if panel_showing else Constants.CL_ALL
    $Structures.visible = !panel_showing
    $UI/HUD.visible = !panel_showing
    if (panel_showing or not _rotation_controls_showing):
        $UI/RotationControls.hide()
    else:
        $UI/RotationControls.show()
  
func _on_lightingModeChanged(lightingMode):
    match lightingMode:
        LightingMode.SUNLIGHT:
            $WorldEnvironment.environment = _sunlight_env
        LightingMode.SUNSET:
            $WorldEnvironment.environment = _sunset_env
        LightingMode.MOONLIGHT:
            $WorldEnvironment.environment = _moonlight_env
    $MainLight.light_color =     $WorldEnvironment.environment.ambient_light_color.lightened(0.5)
    var sky: ProceduralSky = $WorldEnvironment.environment.background_sky
    var sun_pos: Vector3 = Utils3D.rot2looking_at(Vector2(sky.sun_longitude, sky.sun_latitude))
    $MainLight.look_at_from_position(1000*sun_pos, Vector3.ZERO, Vector3.UP)
