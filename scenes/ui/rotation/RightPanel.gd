class_name RightPanel
extends PanelContainer

func _ready():
    MessageBus.subscribe(self, Messages.AUTO_ROTATE_RIGHT, '_on_autoRotateRight')
    MessageBus.subscribe(self, Messages.ROTATE_LEFT, '_on_rotateLeft')
    MessageBus.subscribe(self, Messages.AUTO_ROTATE_LEFT, '_on_autoRotateLeft')

func _on_autoRotateRight(on: bool):
    if on:
        $VBoxContainer/AutoRotateCheckBox.pressed = true

func _on_rotateLeft(on: bool):
    $VBoxContainer/AutoRotateCheckBox.pressed = false

func _on_autoRotateLeft(on: bool):
    if on:
        $VBoxContainer/AutoRotateCheckBox.pressed = false

func _on_RightButton_button_down():
    $VBoxContainer/AutoRotateCheckBox.pressed = false    
    MessageBus.publish(Messages.AUTO_ROTATE_LEFT, false)
    MessageBus.publish(Messages.ROTATE_RIGHT, true)

func _on_RightButton_button_up():
    MessageBus.publish(Messages.ROTATE_RIGHT, false)

func _on_AutoRotateCheckBox_toggled(button_pressed):
    MessageBus.publish(Messages.AUTO_ROTATE_RIGHT, button_pressed)
