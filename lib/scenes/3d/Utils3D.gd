extends Node

func any_perpendicular(vec: Vector3) -> Vector3:
    var v1 = Vector3(0, vec.z, -vec.y)
    var v2 = Vector3(-vec.z, 0, vec.x)
    var v3 = Vector3(-vec.y, vec.x, 0)
    var perp = v1 if v1.length() > v2.length() else v2
    perp = perp if perp.length() > v3.length() else v3
    return perp
   
func rot2looking_at(rot: Vector2) -> Vector3:
    return Vector3.FORWARD.rotated(Vector3.RIGHT, deg2rad(rot.y)).rotated(Vector3.UP, -deg2rad(rot.x))

func rot2basis(rot: Vector2) -> Basis:
    var basis: Basis = Basis(Vector3.RIGHT, Vector3.UP, Vector3.BACK)
    return basis.rotated(Vector3.RIGHT, deg2rad(rot.y)).rotated(Vector3.UP, -deg2rad(rot.x))

func looking_at2rot(looking_at: Vector3):
    var phi = 90 - rad2deg(looking_at.angle_to(Vector3.UP))
        
    var v =looking_at.cross(Vector3.UP)
    var t = rad2deg(v.angle_to(Vector3.RIGHT))
    var theta: float
    if v.project(Vector3.FORWARD).z > 0:
        theta = t
    else:
        theta = 360 - t
    return Vector2(theta, phi)
    
func basis2rot(basis: Basis) -> Vector2:
    return looking_at2rot(-basis.z)
    
