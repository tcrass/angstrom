extends Node

func alter_brightness(color: Color, percent: float):
    if percent == 0:
        return color
    else:
        if percent < 1:
            return color.darkened(1 - percent)
        else: 
            return color.lightened(percent - 1)
        
